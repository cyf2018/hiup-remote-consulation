# 功能
- 远程医疗平台接口交互---接收提醒服务
- 远程医疗平台接口交互---推送患者基本信息和病例文书信息

# 约定
- 文件编码UTF-8
- 编译环境JDK8

## 使用方法说明
- 配置数据源，修改 config/application-db.properties
- 配置调用接口地址 config/config.properties
- 配置医院ID & 医院域ID映射 config/config.properties
- 执行start.bat （如启动闪退，编辑文件修改jdk路径）

- 项目访问地址：http://localhost:9199/dj/patient/remind 
- API测试地址： http://localhost:9199/dj/swagger-ui.html

- 请求方式：POST 请求
- 请求参数：JSON数据：
        {
            "AccessToken": "凭证",
            "HospitalID": "10001",
            "HOSPITAL_Name": "医疗机构名称",
            "IdentityID":"440724195106134028",
            "InHospitalID":"9705142873",
            "OutHospitalID":"24335142873",
            "CardNo":"9705",
            "ApplicationID":"01A54D",
            "ApplicationName":"王小明"
        }
- 返回结果： 
            成功： 
            {
              "faultContext": "",// 请求成功内容为空
              "isSuccess": true
            }
            
            2018-09-03 13:36:29.924 [http-nio-9099-exec-3] INFO  com.controller.PatientController - 推送【患者基本信息信息、临床诊疗信息 和 患者病历文书信息】全部成功!!!
            2018-09-03 13:36:29.924 [http-nio-9099-exec-3] INFO  com.controller.PatientController - push data success!!!耗时: 7427 毫秒
            
            失败： 
            {
                "faultContext": "失败原因描述",
                "isSuccess": true
            }
            
# 注意事项 
- 

# 已知问题
-

#TO DO
-



-- 测试数据

- 主数据测试数据
// String pathologyData = "{\"code\":0,\"result\":{\"personfinalstatus\":{\"effective_time\":\"20181130121817\",\"residential_address\":\"广州市越秀区医国街100号601房\",\"admissions_doctor_id\":\"02238N\",\"registered_address\":\"广州市越秀区医国街100号601房\",\"contact_phone\":\"13802746954\",\"date_of_birth\":\"19361218000000\",\"discharge_date\":\"20180404102135\",\"insurance_no\":\"0020021803131845\",\"patient_class\":\"2\",\"referring_doctor\":\"肖英莲\",\"shift_cause\":\"患者出院\",\"study_time\":\"20181130121817\",\"creator_by_id\":\"20120920000001\",\"received_time\":\"20181130121817\",\"relevance_id_01\":\"ZY010030428446^2.16.840.1.113883.4.487.2.1.4.4\",\"visit_flow_domain\":\"2.16.840.1.113883.4.487.2.1.4.4\",\"admissions_doctor\":\"肖英莲\",\"his_visit_domain_id\":\"2.16.840.1.113883.4.487.2.1.4.4\",\"empi\":\"c306fac3-1e42-4970-974e-1333bae746ad\",\"finally_status_code\":\"21\",\"referring_doctor_id\":\"02238N\",\"shift_type\":\"21\",\"visit_flow_id\":\"ZY010030428446\",\"his_visit_id\":\"ZY010030428446\",\"pat_nurse_name\":\"冯琪\",\"pat_category\":\"1\",\"admit_dept\":\"0304\",\"tend_level\":\"护理医嘱停止\",\"his_id\":\"0030428446\",\"admit_date\":\"20180330182803\",\"contact_relations\":\"家庭内其他关系\",\"contact_person\":\"石小菁\",\"consultation_doctor_id\":\"02238N\",\"date_created\":\"20180330182939\",\"consultation_doctor\":\"肖英莲\",\"gender_cd\":\"1\",\"discharge_dept\":\"0304\",\"finally_status\":\"患者出院\",\"insurance_type\":\"2\",\"his_domain_id\":\"2.16.840.1.113883.4.487.2.1.4\",\"patient_domain_id\":\"2.16.840.1.113883.4.487.2.1.4\",\"relevance_id_02\":\"0030428446^2.16.840.1.113883.4.487.2.1.4\",\"patient_id\":\"0030428446\",\"name\":\"叶翠球\",\"identity_no\":\"440104193612181946\",\"pat_nurse_code\":\"00580K\"},\"searchResponses\":[{\"sysinfoList\":{\"childrenCode\":\"1\",\"esIndex\":\"xds.ur2.risbg_1.0.0.0\",\"esIp\":\"168.168.253.188\",\"esPort\":\"9300\",\"esType\":\"xds.ur2.risbg\",\"isConfiged\":\"1\",\"isMajor\":\"1\",\"mdhcSetSubcode\":\"HDSD00.05_02\",\"pk\":15865,\"sysDomainId\":\"2.16.840.1.113883.4.487.2.1.18\",\"sysName\":\"检查报告\",\"sysNote\":\"检查报告\",\"sysPayLoadType\":\"XDS.UR2.RISBG\",\"sysVersion\":\"1.0.0.0\",\"xmlReceiveTime\":\"2018-09-05 16:21:49\",\"xmlUid\":\"AWWohiCfK6v3gsdZtcpe\"},\"values\":[{\"effective_time\":\"20180404102236\",\"modality\":\"CT\",\"ELAPSEMHIS\":\"既往史\",\"DJ_HH_DE02_10_071_00\":\"现病史\",\"DJ_CAS_DE04_01_119_00\":\"主诉\",\"deprecated\":\"0\",\"examparameter\":\"\",\"DJ_DASC_DE02_01_039_34\":\"叶翠球\",\"doc_name\":\"检查报告\",\"authordomainname\":\"RIS\",\"exammedicaine\":\"\",\"pishpvalue\":\"\",\"examineno\":\"1091813\",\"DJ_LE_DE04_50_132_00\":\"    肝脏形态大小正常，肝叶比例适中，边缘光整。肝实质密度不均，见多发大小不等囊性密度影，边界可见，较大者约20mmX21mm。肝脏S3见大小约10mmX17mm稍低密度区，边界欠清。    肝内外胆道无扩张，胆囊稍大，壁增厚，胆囊内见多发高密度影。    胰腺形态、大小正常，胰管无扩张。脾无肿大，脾门及胃周未见侧枝血管。腹腔无积液，肠系膜区和腹膜后未见淋巴结肿大。    双肾皮髓质变薄，肾窦密度稍高。双肾见多发囊性低密度灶，左肾皮质处见钙化影。    右侧肾上腺外侧肢及左侧肾上腺内侧肢增厚，见结节影。    冠状动脉、主动脉、脾动脉、双肾动脉可见钙化。    心包可见少量水样密度影。    双侧后胸壁胸膜肥厚，见少许弧形液性密度影。\",\"patient_birth\":\"19361218\",\"version\":\"1.0.0.0\",\"DJ_HI_DE08_10_013_14\":\"CT\",\"studydepartmen\":\"\",\"dicomaccessnum\":\"\",\"examinename\":\"上腹部 CT平扫+三维\",\"diagnosiscode\":\"\",\"DJ_DASC_DE02_01_026_00\":\"81岁\",\"consultdocid\":\"\",\"eissuggestion\":\"\",\"examineemployee\":\"\",\"DJ_ID_DE01_00_014_00\":\"0030428446\",\"identifyno\":\"440104193612181946\",\"DJ_LE_DE04_50_133_02\":\"20180404102236\",\"study_time\":\"20180403200812\",\"DJ_DASC_DE02_01_039_116\":\"万向飞\",\"consultdocname\":\"\",\"DJ_ID_DE01_00_008_07\":\"20200940726\",\"modalityname\":\"\",\"diagnosistitle\":\"放射学诊断\",\"his_visit_id\":\"ZY010030428446\",\"effectivetime\":\"20180404102236\",\"patient_name\":\"叶翠球\",\"DJ_DASC_DE02_01_040_00\":\"F\",\"pisbodypart\":\"\",\"file_meta_list\":\"[{\\\"file_size\\\":3395,\\\"file_uid\\\":\\\"2018040244420182\\\",\\\"mime_type\\\":\\\"text/xml\\\"}]\",\"patientsexcode\":\"F\",\"DJ_CSS_WS04_30_905_01\":\"上腹部\",\"diagnosiscodesystem\":\"\",\"documentuniqueid\":\"c245b7d81db84bd78bb918b056e3d9b7\",\"DJ_HI_DE08_10_026_08\":\"消化内科一区\",\"person_info_field\":{\"effective_time\":\"20181130121817\",\"residential_address\":\"广州市越秀区医国街100号601房\",\"DJ_HI_DE08_10_026_19\":\"平素身体健康状况一般。患有高血压病史10余年，平素服用“厄贝沙坦氢氯噻嗪 2# QD、欣洛平 1# TID”控制血压，平素血压不详；患有痛风病史10余年，急性发作时服用双氯芬酸钠缓释胶囊；3年余前于广州市第一人民医院行白内障手术。否认患有糖尿病，否认患有冠心病。否认有肝炎、结核等传染病史，否认外伤史，无食物、药物过敏史。预防接种史不详。\",\"contact_phone\":\"13802746954\",\"discharge_date\":\"20180404102135\",\"insurance_no\":\"0020021803131845\",\"patient_class\":\"2\",\"referring_doctor\":\"肖英莲\",\"DJ_MD_DE05_01_025_11\":\"消化道出血\",\"shift_cause\":\"患者出院\",\"creator_by_id\":\"20120920000001\",\"relevance_id_01\":\"ZY010030428446^2.16.840.1.113883.4.487.2.1.4.4\",\"admissions_doctor\":\"肖英莲\",\"empi\":\"c306fac3-1e42-4970-974e-1333bae746ad\",\"finally_status_code\":\"21\",\"shift_type\":\"21\",\"pat_category\":\"1\",\"tend_level\":\"护理医嘱停止\",\"DJ_ME_DE05_10_172_02\":\"1.胃十二指肠复合溃疡出血（胃癌待排）,2.痛风,3.慢性肾脏病4期,4.慢性胆囊炎,5.胆囊结石,6.双侧肾上腺增生,7.肾脏、肝脏多发囊肿\",\"his_id\":\"0030428446\",\"consultation_doctor_id\":\"02238N\",\"consultation_doctor\":\"肖英莲\",\"gender_cd\":\"1\",\"DJ_ME_DE05_10_109_02\":\"1.消化道出血\",\"discharge_dept\":\"0304\",\"finally_status\":\"患者出院\",\"insurance_type\":\"2\",\"patient_id\":\"0030428446\",\"name\":\"叶翠球\",\"identity_no\":\"440104193612181946\",\"pat_nurse_code\":\"00580K\",\"admissions_doctor_id\":\"02238N\",\"registered_address\":\"广州市越秀区医国街100号601房\",\"date_of_birth\":\"19361218000000\",\"study_time\":\"20181130121817\",\"DJ_HH_DE02_10_071_00\":\"患者10天前自诉服用“糯米粿”后出现食欲下降，伴有胸闷，无发热，无腹痛、腹胀、黑便，无恶心、呕吐，自行服用“安胃止痛胶囊、救心丹”，症状无明显缓解，患者1天仅进食小碗粥。4天前患者无明显诱因下出现排柏油样黑便4次，不成形，量少，伴胸闷、腹痛，无呕血、咯血。1天前于当地医院查血红蛋白56g/L，考虑消化道出血可能，遂至我院急诊就诊，查血红蛋白51g/L，予禁食、补液、抑酸、输血治疗。现为进一步治疗收入我科。自起病以来，患者精神、食欲、睡眠较差，未解大便，小便正常，近期体重下降，具体不详。\",\"received_time\":\"20181130121817\",\"visit_flow_domain\":\"2.16.840.1.113883.4.487.2.1.4.4\",\"his_visit_domain_id\":\"2.16.840.1.113883.4.487.2.1.4.4\",\"referring_doctor_id\":\"02238N\",\"visit_flow_id\":\"ZY010030428446\",\"his_visit_id\":\"ZY010030428446\",\"pat_nurse_name\":\"冯琪\",\"admit_dept\":\"0304\",\"admit_date\":\"20180330182803\",\"contact_relations\":\"家庭内其他关系\",\"contact_person\":\"石小菁\",\"date_created\":\"20180330182939\",\"his_domain_id\":\"2.16.840.1.113883.4.487.2.1.4\",\"patient_domain_id\":\"2.16.840.1.113883.4.487.2.1.4\",\"relevance_id_02\":\"0030428446^2.16.840.1.113883.4.487.2.1.4\"},\"patientregistertime\":\"20180403153000\",\"auditon\":\"20180404102236\",\"doc_id\":\"2018040244420182\",\"DJ_CAE_DE04_30_018_01\":\"CT\",\"patientname\":\"叶翠球\",\"authenticatorpersonid\":\"002084\",\"machineroomname\":\"\",\"DJ_MD_DE05_01_058_00\":\"20180404102236\",\"patient_sex\":\"女\",\"devicename\":\"\",\"sectiontitle\":\"放射学表现\",\"reportno\":\"1\",\"DJ_LE_DE04_50_131_00\":\"1.慢性胆囊炎、胆囊多发结石2.双侧肾上腺增生3.双肾萎缩并囊肿4.肝脏多发囊肿。肝脏S3稍低密度灶，建议增强扫描5.冠状动脉、主动脉、脾动脉、双肾动脉钙化，原因考虑粥样硬化6.心包、双侧胸腔少量积液\",\"DJ_DASC_DE02_01_039_73\":\"杨有优\",\"otherhospitalname\":\"广州中山大学附属第一医院\",\"authorpersonname\":\"万向飞\",\"tablecontext\":\"\",\"applydepartmentsid\":\"0304\",\"reporton\":\"20180403200812\",\"applydepartmentsname\":\"消化内科一区\",\"patienttypecodesystem\":\"2.16.840.1.113883.4.487.2.1.1.1.13\",\"authordomainid\":\"2.16.840.1.113883.4.487.2.1.18\",\"his_id\":\"0030428446\",\"DJ_DASC_DE02_01_060_00\":\"住院\",\"ifotherhospital\":\"0\",\"studyroom\":\"\",\"studybedroom\":\"004\",\"telephone\":\"13802746954\",\"document_unique_id\":\"2018040244420182\",\"patientsexname\":\"女\",\"authorpersonid\":\"\",\"report_time\":\"20180403200812\",\"authenticatordomainname\":\"RIS\",\"requestid\":\"\",\"diagnosisstatuscode\":\"PO\",\"diagnosismethodcode\":\"\",\"injectionmethod\":\"\",\"reportconclusion\":\"1.慢性胆囊炎、胆囊多发结石2.双侧肾上腺增生3.双肾萎缩并囊肿4.肝脏多发囊肿。肝脏S3稍低密度灶，建议增强扫描5.冠状动脉、主动脉、脾动脉、双肾动脉钙化，原因考虑粥样硬化6.心包、双侧胸腔少量积液\",\"DJ_CSS_WS08_10_025_19\":\"0304\",\"dicomstudytime\":\"\",\"rowversion\":\"\",\"document_domain_id\":\"2.16.840.1.113883.4.487.2.1.18\",\"examineon\":\"20180403160054\",\"hisid\":\"0030428446\",\"patientsexcodesystem\":\"2.16.840.1.113883.4.487.2.1.1.1.9\",\"received_time\":\"20180404102300\",\"remark\":\"\",\"DJ_ID_DE01_00_018_00\":\"1091813\",\"his_visit_domain_id\":\"2.16.840.1.113883.4.487.2.1.4.4\",\"title\":\"数字化放射诊断报告书\",\"repeatnumber\":\"\",\"pay_load_type\":\"XDS.UR2.RISBG\",\"clinicaldiagnose\":\"消化道出血\",\"orderiis\":\"20200940726\",\"accnumberlist\":\"[{2018040244420182}]\",\"department\":\"消化内科一区\",\"patienttype\":\"住院\",\"dicomstudyuidlist\":\"[{1.2.840.113820.104.4756.120180403153539}]\",\"patientbirth\":\"19361218\",\"reportii\":\"2018040244420182\",\"reportdesc\":\"    肝脏形态大小正常，肝叶比例适中，边缘光整。肝实质密度不均，见多发大小不等囊性密度影，边界可见，较大者约20mmX21mm。肝脏S3见大小约10mmX17mm稍低密度区，边界欠清。    肝内外胆道无扩张，胆囊稍大，壁增厚，胆囊内见多发高密度影。    胰腺形态、大小正常，胰管无扩张。脾无肿大，脾门及胃周未见侧枝血管。腹腔无积液，肠系膜区和腹膜后未见淋巴结肿大。    双肾皮髓质变薄，肾窦密度稍高。双肾见多发囊性低密度灶，左肾皮质处见钙化影。    右侧肾上腺外侧肢及左侧肾上腺内侧肢增厚，见结节影。    冠状动脉、主动脉、脾动脉、双肾动脉可见钙化。    心包可见少量水样密度影。    双侧后胸壁胸膜肥厚，见少许弧形液性密度影。\",\"dicommodality\":\"CT\",\"DJ_LE_DE04_50_140_01\":\"20180403160054\",\"bodyparts\":\"上腹部\",\"patienttypecode\":\"1\",\"authenticatordomainid\":\"2.16.840.1.113883.4.487.2.1.18\",\"encounterii\":\"2018040244420182\",\"his_domain_id\":\"2.16.840.1.113883.4.487.2.1.4\",\"auditemployee\":\"杨有优\",\"patientage\":\"81岁\",\"image_meta_list\":\"[{\\\"dicom_accession_num\\\":\\\"\\\",\\\"dicom_body_part\\\":\\\"上腹部\\\",\\\"dicom_device_name\\\":\\\"\\\",\\\"dicom_diagnosis_method_code\\\":\\\"\\\",\\\"dicom_machine_room_name\\\":\\\"\\\",\\\"dicom_modality\\\":\\\"CT\\\",\\\"dicom_num\\\":\\\"1091813\\\",\\\"dicom_study_time\\\":\\\"\\\",\\\"dicom_study_uid\\\":\\\"2018040244420182\\\"}]\"}]}]}}";
- 影像接口测试数据

    
    