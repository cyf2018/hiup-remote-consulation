package com.hiup.mapper;

import com.hiup.entity.TblRemoteConsult;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会诊申请提醒远程查阅 Mapper 接口
 * </p>
 *
 * @author cyf
 * @since 2018-12-19
 */
public interface TblRemoteConsultMapper extends BaseMapper<TblRemoteConsult> {

}
