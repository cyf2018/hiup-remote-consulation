package com.hiup.mapper;

import com.hiup.entity.Person;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 病人基本信息主表 Mapper 接口
 * </p>
 *
 * @author cyf
 * @since 2018-12-19
 */
public interface PersonMapper extends BaseMapper<Person> {

}
