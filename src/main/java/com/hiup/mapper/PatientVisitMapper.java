package com.hiup.mapper;

import com.hiup.entity.PatientVisit;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 病人就诊信息 Mapper 接口
 * </p>
 *
 * @author cyf
 * @since 2018-12-19
 */
public interface PatientVisitMapper extends BaseMapper<PatientVisit> {

}
