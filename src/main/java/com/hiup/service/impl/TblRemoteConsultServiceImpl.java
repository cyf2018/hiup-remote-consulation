package com.hiup.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hiup.config.DbContextHolder;
import com.hiup.constant.DBTypeEnum;
import com.hiup.entity.TblRemoteConsult;
import com.hiup.mapper.TblRemoteConsultMapper;
import com.hiup.service.TblRemoteConsultService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * <p>
 * 会诊申请提醒远程查阅 服务实现类
 * </p>
 *
 * @author cyf
 * @since 2018-12-19
 */
@Service
public class TblRemoteConsultServiceImpl extends ServiceImpl<TblRemoteConsultMapper, TblRemoteConsult> implements TblRemoteConsultService {
    private static final Logger log = LoggerFactory.getLogger(TblRemoteConsultServiceImpl.class);

    @Override
    public boolean insert(TblRemoteConsult consult) {
        DbContextHolder.setDbType(DBTypeEnum.db1);
        consult.setId(UUID.randomUUID().toString());
        boolean insertResult = false;
        try {
            insertResult = super.insert(consult);
        } catch (Exception e) {
            log.error("insert consult error!!! {},{}",e.getMessage(),e);
        }
        if(insertResult){
            log.debug("insert consult success!!!,the id is {}",consult.getId());
            log.info("insert consult success!!!,the id is {}",consult.getId());
        }  else {
            log.debug("insert consult error!!!");
            log.info("insert consult error!!!");
        }
        return insertResult;
    }

}
