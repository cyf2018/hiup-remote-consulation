package com.hiup.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hiup.config.DbContextHolder;
import com.hiup.constant.DBTypeEnum;
import com.hiup.constant.GlobalConstant;
import com.hiup.entity.Person;
import com.hiup.mapper.PersonMapper;
import com.hiup.service.PersonService;
import com.hiup.utils.BeanUtil;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 病人基本信息主表 服务实现类
 * </p>
 *
 * @author cyf
 * @since 2018-12-19
 */
@Service
public class PersonServiceImpl extends ServiceImpl<PersonMapper, Person> implements PersonService {

    /**
     * 查询person ，如果多个person，取创建时间最近的一个。
     *
     * @param map
     * @return
     */
    public Person selectOne(Map<String, Object> map) {
        DbContextHolder.setDbType(DBTypeEnum.db2);
        List<Person> personList = selectByMap(map);
        if (CollectionUtils.isEmpty(personList)) {
            return null;
        }

        return BeanUtil.getMaxFieldObj(personList, GlobalConstant.DATE_CREATED);
    }
}
