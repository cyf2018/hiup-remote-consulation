package com.hiup.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hiup.config.DbContextHolder;
import com.hiup.constant.DBTypeEnum;
import com.hiup.constant.GlobalConstant;
import com.hiup.entity.PatientVisit;
import com.hiup.mapper.PatientVisitMapper;
import com.hiup.service.PatientVisitService;
import com.hiup.utils.BeanUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 病人就诊信息 服务实现类
 * </p>
 *
 * @author cyf
 * @since 2018-12-19
 */
@Service
public class PatientVisitServiceImpl extends ServiceImpl<PatientVisitMapper, PatientVisit> implements PatientVisitService {
    private static final Logger log = LoggerFactory.getLogger(PatientVisitServiceImpl.class);

    public PatientVisit selectOne(String personId) {
        DbContextHolder.setDbType(DBTypeEnum.db2);
        if (StringUtils.isEmpty(personId)) {
            log.info("the personId is null.");
            return new PatientVisit();
        }
        return getPatientVisit(personId);
    }

    private PatientVisit getPatientVisit(String personId) {
        log.debug("患者基本信息分表personId是: {}", personId);
        Map<String, Object> map = new HashMap<>();
        map.put("person_id", personId);
        List<PatientVisit> patientVisits = null;
        try {
            patientVisits = selectByMap(map);
        } catch (Exception e) {
            log.error("查询患者基本信息失败.{},{}",e.getMessage(),e);
            return new PatientVisit();
        }
        log.debug("患者基本信息的分表patientVisit集合为: {}", patientVisits);
        return BeanUtil.getMaxFieldObj(patientVisits, GlobalConstant.ADMIT_DATE);
    }

}
