package com.hiup.service;

import com.hiup.entity.Person;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 病人基本信息主表 服务类
 * </p>
 *
 * @author cyf
 * @since 2018-12-19
 */
public interface PersonService extends IService<Person> {

}
