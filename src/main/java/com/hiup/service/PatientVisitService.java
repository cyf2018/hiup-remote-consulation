package com.hiup.service;

import com.hiup.entity.PatientVisit;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 病人就诊信息 服务类
 * </p>
 *
 * @author cyf
 * @since 2018-12-19
 */
public interface PatientVisitService extends IService<PatientVisit> {

}
