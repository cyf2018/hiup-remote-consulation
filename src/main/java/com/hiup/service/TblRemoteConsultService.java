package com.hiup.service;

import com.hiup.entity.TblRemoteConsult;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会诊申请提醒远程查阅 服务类
 * </p>
 *
 * @author cyf
 * @since 2018-12-19
 */
public interface TblRemoteConsultService extends IService<TblRemoteConsult> {

}
