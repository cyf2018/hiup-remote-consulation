package com.hiup.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 文件内容
 * @author YFC
 * @create 2018-12-20 上午 10:37
 */
@Getter
@Setter
@ToString
public class PatientFileDataVo {

    /**
     * 文件编号
     */
    private String FileID;

    /**
     * 文件大类
     */
    private int FileCategory;

    /**
     * 文件名称
     */
    private String FileName;

    /**
     * 文件二进制流
     */
    private String FileData;

    /**
     * 文件扩展名
     */
    private String FileExtension;

    /**
     * 文件记录时间
     */
    private String RecordTime;
}
