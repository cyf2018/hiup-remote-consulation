package com.hiup.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * 影像
 *
 * @author YFC
 * @create 2018-12-21 上午 1:51
 */
@Setter
@Getter
@ToString
public class ImageVo {

    /**
     * 影像接口数据
     */
    private List<ImageDataVo> studyList;

    /**
     * 结果标识 0-成功 1-失败
     */
    private String resultCode;

    /**
     * 错误消息，成功无返回内容
     */
    private String errorMsg;

}
