package com.hiup.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * 患者病历信息vo
 *
 * @author YFC
 * @create 2018-12-20 上午 3:59
 */
@Setter
@Getter
@ToString
public class PatientDocumentVo {

    /**
     *  患者信息相关
     */
    private PatientInfo patientInfo;

    /**
     * 就诊号
     */
    private String inhos_no;

    /**
     * 流水号
     */
    private String his_flow_id;

    /**
     * 就诊类型 1:门诊,2:住院,3:急诊,4:其他
     */
    private int inhos_type;

    /**
     * 主诉
     */
    private String chief_complaint;

    /**
     * 现病史
     */
    private String history_of_presentillness;

    /**
     * 既往史
     */
    private String past_history;


    /**
     * 临床诊断
     */
    private String clinical_diagnosis;

    /**
     * 影像数据
     */
    private List<ImageDataVo> medicalRecords;

    /**
     * 文件内容
     */
    private List<PatientFileDataVo> patientFiles;


    /**
     * 凭证
     */
    private String AccessToken;

    /**
     * 机构编号
     */
    private String org_code;

    /**
     * 机构名称
     */
    private String org_name;

    /**
     * 系统类型 1-HIS,2-EMR,3-LIS, 4-RIS,5-PACS,6-DJHIUP,9-OTHERPT
     */
    private int sys_type;

    /**
     * 机构内患者ID
     */
    private String patient_id;

    /**
     * 患者姓名
     */
    private String patient_name;

    /**
     * 身份证号
     */
    private String identity_num;

}
