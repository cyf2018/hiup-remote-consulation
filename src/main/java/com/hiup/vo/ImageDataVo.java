package com.hiup.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 影像数据内容
 *
 * @author YFC
 * @create 2018-12-20 上午 10:36
 */
@Setter
@Getter
@ToString
public class ImageDataVo {

    /**
     * 影像号  调用影像接口参数名为 patientID,
     * 实际字段名称为 DicomPatientID
     */
    private String patientID;

    /**
     * 存储编号
     */
    private String accNum;

    /**
     * 检查号
     */
    private String studyUid;

    /**
     * PACSServer/影像设备AE
     */
    // 添加JsonProperty 是为了解决json转obj出现异常，Unrecognized field, not marked as ignorable
    @JsonProperty(value = "AE")
    private String AE;

    /**
     * PACSServer/影像设备IP
     */
    @JsonProperty(value = "IP")
    private String IP;

    /**
     * PACSServer/影像设备PORT
     */
    private String port;

    /**
     * 检查类型
     */
    private String examType;

    /**
     * 检查部位
     */
    private String examBodyPart;

    /**
     * 检查方法
     */
    private String examMethod;

    /**
     * 原单位病理号
     */
    private String Pathological;

    /**
     * 送检医院
     */
    private String SentHospital;

    /**
     * 送检日期
     */
    private String SentTime;

    /**
     * 送检物
     */
    private String SentThing;

    /**
     * 取材部位
     */
    private String CoverageArea;

    /**
     * 大体所见
     */
    private String GeneralSeen;

    /**
     * 免疫组化
     */
    private String Immunity;

    /**
     * 特殊检查&注意事项
     */
    private String SpecialInspection;

}
