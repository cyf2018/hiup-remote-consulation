package com.hiup.vo;

import com.hiup.entity.Person;
import com.hiup.utils.DateUtil;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;

import java.math.BigInteger;
import java.util.Objects;

/**
 * @author YFC
 * @create 2018-12-20 上午 3:16
 */
@ToString
public class PatientInfoVo {

    /**
     * 患者信息相关
     */
    private PatientInfo patientInfo;


    /**
     * 患者性别码
     */
    private int patient_sex;


    /**
     * 身份证件类别编码
     */
    private String identity_type_code;


    /**
     * 地址
     */
    private String home_place;

    /**
     * 联系方式
     */
    private String patient_phone_num;

    /**
     * 年龄
     */
    private int age;

    /**
     *  患者对象
     */
    private Person person;


    /**
     * 凭证
     */
    private String AccessToken;

    /**
     * 机构编号
     */
    private String org_code;

    /**
     * 机构名称
     */
    private String org_name;

    /**
     * 系统类型 1-HIS,2-EMR,3-LIS, 4-RIS,5-PACS,6-DJHIUP,9-OTHERPT
     */
    private int sys_type;

    /**
     * 机构内患者ID
     */
    private String patient_id;

    /**
     * 患者姓名
     */
    private String patient_name;

    /**
     * 身份证号
     */
    private String identity_num;



    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public PatientInfo getPatientInfo() {
        if (patientInfo == null) {
            return new PatientInfo();
        }
        return patientInfo;
    }

    public void setPatientInfo(PatientInfo patientInfo) {
        this.patientInfo = patientInfo;
    }

    public int getPatient_sex() {
        return patient_sex;
    }

    public void setPatient_sex(int patient_sex) {
        this.patient_sex = patient_sex;
    }

    public String getIdentity_type_code() {
        return identity_type_code;
    }

    public void setIdentity_type_code(String identity_type_code) {
        this.identity_type_code = identity_type_code;
    }

    public String getHome_place() {
        return home_place;
    }

    public void setHome_place(String home_place) {
        this.home_place = home_place;
    }

    public String getPatient_phone_num() {
        return patient_phone_num;
    }

    public void setPatient_phone_num(String patient_phone_num) {
        this.patient_phone_num = patient_phone_num;
    }

    public int getAge() {
        if (Objects.isNull(age)) {
            return age;
        }
        if (age <= 0) {
            if (StringUtils.isEmpty(getPatientInfo().getIdentity_num())) {
                this.age = 0;
            } else {
                try {
                    BigInteger age = new BigInteger(DateUtil.getYear())
                            .subtract(new BigInteger(getPatientInfo().getIdentity_num()
                                    .substring(6, 10)));
                    this.age = Integer.parseInt(age.toString());
                } catch (NumberFormatException e) {
                    this.age = 0;
                }
            }
        }
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    public String getAccessToken() {
        return AccessToken;
    }

    public void setAccessToken(String accessToken) {
        AccessToken = accessToken;
    }

    public String getOrg_code() {
        return org_code;
    }

    public void setOrg_code(String org_code) {
        this.org_code = org_code;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public int getSys_type() {
        return sys_type;
    }

    public void setSys_type(int sys_type) {
        this.sys_type = sys_type;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getPatient_name() {
        return patient_name;
    }

    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }

    public String getIdentity_num() {
        return identity_num;
    }

    public void setIdentity_num(String identity_num) {
        this.identity_num = identity_num;
    }
}
