package com.hiup.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author YFC
 * @create 2018-12-20 上午 10:27
 */
@Setter
@Getter
@ToString
public class PatientInfo {

    /**
     * 凭证
     */
    private String AccessToken;

    /**
     * 机构编号
     */
    private String org_code;

    /**
     * 机构名称
     */
    private String org_name;

    /**
     * 系统类型 1-HIS,2-EMR,3-LIS, 4-RIS,5-PACS,6-DJHIUP,9-OTHERPT
     */
    private int sys_type;

    /**
     * 机构内患者ID
     */
    private String patient_id;

    /**
     * 患者姓名
     */
    private String patient_name;

    /**
     * 身份证号
     */
    private String identity_num;

}
