package com.hiup.config;

import com.djhu.hiup.documentum.client.DocumentService;
import com.djhu.hiup.documentum.client.future.imp.DocumentServiceImp;
import com.hiup.config.comment.InterComment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * 文件服务器相关配置
 *
 * @author YFC
 * @create 2018-12-21 上午 12:53
 */
@Configuration
public class DocumentConfig {

    @Autowired
    private InterComment interComment;

    @Bean(initMethod = "init")
    public DocumentService documentService(){
        DocumentServiceImp documentService = new DocumentServiceImp();
        documentService.setServerip(interComment.getServerip());
        documentService.setServerport(Integer.parseInt(interComment.getServerport()));
        documentService.setLocalpath(interComment.getLocalpath());
        return documentService;
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }



}
