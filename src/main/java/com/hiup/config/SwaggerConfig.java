package com.hiup.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *  Rest API
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
	public Docket userApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("远程接口服务")
				.apiInfo(apiInfo())
				.select()
				//为当前包路径
				.apis(RequestHandlerSelectors.basePackage("com.hiup.web"))
				.paths(PathSelectors.any()).build();
	}
	// 预览地址:swagger-ui.html
	//构建 api文档的详细信息函数,注意这里的注解引用的是哪个
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				//页面标题
				.title("远程接口调用服务API")
				//.termsOfServiceUrl("http://localhost:9099/dj/patient/remind")
				//创建人
//				.contact(new Contact("yufei.cao ", "http://localhost:9099/dj/patient/remind", "871263057@qq.com"))
				//版本号
				.version("1.1")
//				.description("描述")
				.build();
	}
}
