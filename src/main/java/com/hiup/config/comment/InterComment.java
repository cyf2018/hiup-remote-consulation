package com.hiup.config.comment;

import com.hiup.utils.JSONUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 接口相关配置
 *
 * @author YFC
 * @create 2018-12-20 上午 7:15
 */
@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "inter.config")
public class InterComment {

    /**
     * 主数据接口
     */
    private String pathologyUrl;

    /**
     * 影像接口
     */
    private String imageUrl;

    // ---------------------------------

    /**
     * 患者信息到远程会诊
     */
    private String patientInfoUrl;

    /**
     * 病历文书到远程会诊
     */
    private String patientFileUrl;


    // ---------------------------------

    /**
     * 文件服务器接口
     */
    private String serverip;
    private String serverport;
    private String localpath;


    @Override
    public String toString() {
        Map<String, Object> map = new HashMap<>();
        map.put("serverip", serverip);
        map.put("serverport", serverport);
        map.put("localpath", localpath);
        map.put("pathologyUrl", pathologyUrl);
        map.put("imageUrl", imageUrl);
        map.put("patientInfoUrl", patientInfoUrl);
        map.put("patientFileUrl", patientFileUrl);
        return JSONUtil.objectToJson(map);
    }

}
