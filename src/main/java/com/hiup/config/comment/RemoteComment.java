package com.hiup.config.comment;

import com.hiup.utils.JSONUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 远程接口相关配置
 *
 * @author YFC
 * @create 2018-12-20 上午 5:54
 */
@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "remote.config")
public class RemoteComment {

    /**
     * 系统类型
     */
    private int sysType;

    /**
     * 文件扩展名
     */
    private String fileExtension;

    @Override
    public String toString() {
        Map<String, Object> map = new HashMap<>();
        map.put("sysType", sysType);
        map.put("fileExtension", fileExtension);
        return JSONUtil.objectToJson(map);
    }


}
