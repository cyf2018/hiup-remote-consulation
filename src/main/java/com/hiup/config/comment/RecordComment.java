package com.hiup.config.comment;

import com.hiup.utils.JSONUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 病历信息相关配置
 *
 * @author YFC
 * @create 2018-12-20 上午 6:19
 */
@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "record.config")
public class RecordComment {

    /**
     * 住院号或者门诊号
     */
    private String his_id;

    /**
     * 住院号或者门诊号域
     */
    private String his_domain_id;

    /**
     * 住院号或者门诊号流水号
     */
    private String his_visit_id;

    /**
     * 住院号或者门诊号流水号域
     */
    private String his_visit_domain_id;

    /**
     * 获取病历信息开始时间
     */
    private String startDate;

    /**
     * 获取病历信息结束时间
     */
    private String endDate;

    /**
     * 获取病历信息近一周内容 单位:天
     */
    private int sectionDate;


    @Override
    public String toString() {
        Map<String, Object> map = new HashMap<>();
        map.put("his_id", his_id);
        map.put("his_domain_id", his_domain_id);
        map.put("his_visit_id", his_visit_id);
        map.put("his_visit_domain_id", his_visit_domain_id);
        map.put("startDate", startDate);
        map.put("endDate", endDate);
        map.put("sectionDate", sectionDate);
        return JSONUtil.objectToJson(map);
    }
}
