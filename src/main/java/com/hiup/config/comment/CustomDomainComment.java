package com.hiup.config.comment;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author cyf
 * @description
 * @create 2018-12-27 16:13
 **/
@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "remote.config.custom")
public class CustomDomainComment {

    /**
     * 自定义域(即指定域)
     */
    private String domain;

    /**
     * 自定义域是否开启
     */
    private boolean enabled;
}
