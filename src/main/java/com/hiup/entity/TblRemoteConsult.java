package com.hiup.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 会诊申请提醒远程查阅
 * </p>
 *
 * @author cyf
 * @since 2018-12-19
 */
@TableName("TBL_REMOTE_CONSULT")
public class TblRemoteConsult extends Model<TblRemoteConsult> {

    private static final long serialVersionUID = 1L;

    @TableId(type=IdType.INPUT)
    private String id;

    /**
     * 凭证
     */
    @NotNull(message = "凭证不能为空")
    @JsonProperty(value = "AccessToken")
    private String accessToken;

    /**
     * 医疗机构编号
     */
    @NotNull(message = "医院编号不能为空")
    @JsonProperty(value = "HospitalID")
    private String hospitalId;

    /**
     * 医疗机构名称
     */
    @NotNull(message = "医疗机构名称不能为空")
    @JsonProperty(value = "HOSPITAL_Name")
    private String hospitalName;

    /**
     * 患者身份证号
     */
    @NotNull(message = "患者身份证号不能为空")
    @JsonProperty(value = "IdentityID")
    private String identityId;

    /**
     * 住院号
     */
    @JsonProperty(value = "InHospitalID")
    private String inHospitalId;

    /**
     * 门诊号
     */
    @JsonProperty(value = "OutHospitalID")
    private String outHospitalId;

    /**
     * 就诊卡号
     */
    @JsonProperty(value = "CardNo")
    private String cardNo;

    /**
     * 医保卡号
     */
    @JsonProperty(value = "InsuranceNo")
    private String insuranceNo;

    /**
     * 申请用户ID
     */
    @NotNull(message = "申请用户ID不能为空")
    @JsonProperty(value = "ApplicationID")
    private String applicationId;

    /**
     * 申请用户姓名
     */
    @NotNull(message = "申请用户姓名不能为空")
    @JsonProperty(value = "ApplicationName")
    private String applicationName;

    /**
     * 创建时间
     */
    private String createTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }
    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }
    public String getIdentityId() {
        return identityId;
    }

    public void setIdentityId(String identityId) {
        this.identityId = identityId;
    }
    public String getInHospitalId() {
        return inHospitalId;
    }

    public void setInHospitalId(String inHospitalId) {
        this.inHospitalId = inHospitalId;
    }
    public String getOutHospitalId() {
        return outHospitalId;
    }

    public void setOutHospitalId(String outHospitalId) {
        this.outHospitalId = outHospitalId;
    }
    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }
    public String getInsuranceNo() {
        return insuranceNo;
    }

    public void setInsuranceNo(String insuranceNo) {
        this.insuranceNo = insuranceNo;
    }
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }
    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }
    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TblRemoteConsult{" +
        "id=" + id +
        ", accessToken=" + accessToken +
        ", hospitalId=" + hospitalId +
        ", hospitalName=" + hospitalName +
        ", identityId=" + identityId +
        ", inHospitalId=" + inHospitalId +
        ", outHospitalId=" + outHospitalId +
        ", cardNo=" + cardNo +
        ", insuranceNo=" + insuranceNo +
        ", applicationId=" + applicationId +
        ", applicationName=" + applicationName +
        ", createTime=" + createTime +
        "}";
    }
}
