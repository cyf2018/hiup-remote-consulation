package com.hiup.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 病人基本信息主表
 * </p>
 *
 * @author cyf
 * @since 2018-12-19
 */
public class Person extends Model<Person> {
    private static final long serialVersionUID = 1L;

    /**
     * 身份证号默认编码
     */
    public static final String ID_NUMBER_DEFAULT_CODE = "01";


    /**
     * 序号
     */
    @TableId(value = "person_id", type = IdType.INPUT)
    private String personId;

    /**
     * 姓名
     */
    private String givenName;

    /**
     * 中间名
     */
    private String middleName;

    /**
     * 家族名
     */
    private String familyName;

    /**
     * 前缀
     */
    private String prefix;

    /**
     * 后缀
     */
    private String suffix;

    /**
     * 名字类型编码
     */
    private String nameTypeCd;

    /**
     * 姓名
     */
    private String name;

    /**
     * 名字代码
     */
    private String nameCode;

    /**
     * 中文姓名拼音码
     */
    private String nameSpellCode;

    /**
     * 中文姓名五笔码
     */
    private String nameWbCode;

    /**
     * 出生日期
     */
    private Date dateOfBirth;

    /**
     * 出生地所在地省
     */
    private String birthProvince;

    /**
     * 出生地所在地市
     */
    private String birthCity;

    /**
     * 出生地所在区县
     */
    private String birthCounty;

    /**
     * 出生地邮编
     */
    private String birthZip;

    /**
     * 出生地
     */
    private String birthPlace;

    /**
     * 多胞胎
     */
    private String multipleBirthInd;

    /**
     * 出生次序
     */
    private String birthOrder;

    /**
     * 母亲娘家姓
     */
    private String mothersMaidenName;

    /**
     * 社会保险号
     */
    private String ssn;

    /**
     * 身份证号
     */
    private String identityNo;

    /**
     * 健康卡号
     */
    private String healthCard;

    /**
     * 市民卡号
     */
    private String citizenCard;

    /**
     * 医疗证号
     */
    private String medicalCertificate;

    /**
     * 医保个人编号
     */
    private String meicarePerson;

    /**
     * 老人证号
     */
    private String elderCertificate;

    /**
     * 病历号
     */
    private String opcaseno;

    /**
     * 医疗保险号
     */
    private String insuranceNo;

    /**
     * 医疗保险类型
     */
    private String insuranceType;

    /**
     * 医疗保险名称
     */
    private String insuranceName;

    /**
     * 性别编码
     */
    private String genderCd;

    /**
     * 性别编码名
     */
    private String genderName;

    /**
     * 性别编码系统
     */
    private String genderDomain;

    /**
     * 民族编码
     */
    private String ethnicGroupCd;

    /**
     * 民族名称
     */
    private String ethnicName;

    /**
     * 民族编码系统
     */
    private String ethnicDomain;

    /**
     * 种族编码
     */
    private String raceCd;

    /**
     * 种族名称
     */
    private String raceName;

    /**
     * 种族编码系统
     */
    private String raceDomain;

    /**
     * 国籍编码
     */
    private String nationalityCd;

    /**
     * 国籍名称
     */
    private String nationalityName;

    /**
     * 国籍编码系统
     */
    private String nationalityDomain;

    /**
     * 语言编码
     */
    private String languageCd;

    /**
     * 宗教编码
     */
    private String religionCd;

    /**
     * 婚姻状态编码
     */
    private String maritalStatusCd;

    /**
     * 婚姻状态名称
     */
    private String maritalStatusName;

    /**
     * 婚姻状态编码系统
     */
    private String maritalDomain;

    /**
     * 学历编码
     */
    private String degree;

    /**
     * 学历名称
     */
    private String degreeName;

    /**
     * 学历编码系统
     */
    private String degreeDomain;

    /**
     * 邮件地址
     */
    private String email;

    /**
     * 居住地所在地省
     */
    private String homeProvince;

    /**
     * 居住地所在地市
     */
    private String homeCity;

    /**
     * 居住地所在区县
     */
    private String homeCounty;

    /**
     * 居住地邮编
     */
    private String homeZip;

    /**
     * 居住地地址
     */
    private String homeAddress;

    /**
     * 户口所在地省
     */
    private String registeredProvince;

    /**
     * 户口所在地市
     */
    private String registeredCity;

    /**
     * 户口所在地区县
     */
    private String registeredCounty;

    /**
     * 户口所在地邮编
     */
    private String registeredZip;

    /**
     * 户口所在地之
     */
    private String registeredAddress;

    /**
     * 籍贯所在地省
     */
    private String nativeProvince;

    /**
     * 籍贯所在地市
     */
    private String nativeCity;

    /**
     * 工作邮编
     */
    private String workZip;

    /**
     * 单位地址
     */
    private String workAddress;

    /**
     * 地址1
     */
    private String address1;

    /**
     * 邮政编码1
     */
    private String postalCode;

    /**
     * 地址类型编码1
     */
    private String addressTypeCd;

    /**
     * 地址2
     */
    private String address2;

    /**
     * 邮政编码2
     */
    private String postalCode1;

    /**
     * 地址类型编码2
     */
    private String address1TypeCd;

    /**
     * 城市
     */
    private String city;

    /**
     * 省
     */
    private String state;

    /**
     * 国家
     */
    private String country;

    /**
     * 国家编码
     */
    private String countryCode;

    /**
     * 电话号码国家代码
     */
    private String phoneCountryCode;

    /**
     * 国家号码
     */
    private String phoneAreaCode;

    /**
     * 电话号码
     */
    private String phoneNumber;

    /**
     * 分机号
     */
    private String phoneExt;

    /**
     * 电话类型编码
     */
    private String phoneTypeCd;

    /**
     * 电话号码国家代码1
     */
    private String phoneCountryCode1;

    /**
     * 国家号码1
     */
    private String phoneAreaCode1;

    /**
     * 电话号码1
     */
    private String phoneNumber1;

    /**
     * 分机号1
     */
    private String phoneExt1;

    /**
     * 电话类型编码1
     */
    private String phoneTypeCd1;

    /**
     * 公司
     */
    private String company;

    /**
     * 公司联系方式
     */
    private String companycontacts;

    /**
     * 出生地编码
     */
    private String birthplaceCd;

    /**
     * 工作状态
     */
    private String workstatus;

    /**
     * 职业编码
     */
    private String profession;

    /**
     * 职业名称
     */
    private String professionName;

    /**
     * 职业编码系统
     */
    private String professionDomain;

    /**
     * 私人电话
     */
    private String privateNumber;

    /**
     * 家庭电话
     */
    private String homeNumber;

    /**
     * 单位电话
     */
    private String workNumber;

    /**
     * 监护人
     */
    private String guardianPerson;

    /**
     * 保密
     */
    private String vip;

    /**
     * 账户域ID
     */
    private String accountIdentifierDomainId;

    /**
     * 账户
     */
    private String account;

    /**
     * 死亡标识
     */
    private String deathInd;

    /**
     * 死亡事件
     */
    private Date deathTime;

    /**
     * 创建日期
     */
    private Date dateCreated;

    /**
     * 创建者
     */
    private String creatorId;

    /**
     * 修改日期
     */
    private Date dateChanged;

    /**
     * 修改人
     */
    private String changedById;

    /**
     * 废弃日期
     */
    private Date dateVoided;

    /**
     * 废弃标识
     */
    private String voidedById;

    /**
     * 组号
     */
    private String groupNumber;

    /**
     * 血型编码
     */
    private String bloodTypeCd;

    /**
     * RH类型
     */
    private String rhType;

    /**
     * EMPI
     */
    private String empi;

    /**
     * 医院CD
     */
    private String hospitalCd;

    /**
     * 自定义1
     */
    private String custom1;

    /**
     * 流水域名字
     */
    private String custom2;

    /**
     * 流水域ID
     */
    private String custom3;

    /**
     * 流水域类型
     */
    private String custom4;

    /**
     * 自定义5
     */
    private String custom5;

    /**
     * 流水ID^流水域ID
     */
    private String custom6;

    /**
     * 市民卡号
     */
    private String custom7;

    /**
     * 医疗证号
     */
    private String custom8;

    /**
     * 医保个人编号
     */
    private String custom9;

    /**
     * 机构名称
     */
    private String custom10;

    /**
     * 机构域ID
     */
    private String custom11;

    /**
     * 机构类型
     */
    private String custom12;

    /**
     * 老人证号
     */
    private String custom13;

    /**
     * 病历号
     */
    private String custom14;

    /**
     * HIS就诊号
     */
    private String custom15;

    /**
     * 机构内病人ID
     */
    private String custom16;

    /**
     * 社会保险号
     */
    private String custom17;

    /**
     * 健康卡
     */
    private String custom18;

    /**
     * HIS流水号
     */
    private String custom19;

    /**
     * 关联ID^关联域
     */
    private String custom20;

    /**
     * 关联ID^关联域
     */
    private String custom21;

    /**
     * 自定义22
     */
    private String custom22;

    /**
     * 自定义23
     */
    private String custom23;

    /**
     * 病案号
     */
    private String custom24;

    /**
     * 自定义25
     */
    private String custom25;

    /**
     * 自定义26
     */
    private String custom26;

    /**
     * 自定义27
     */
    private String custom27;

    /**
     * 自定义28
     */
    private String custom28;

    /**
     * 自定义29
     */
    private String custom29;

    /**
     * 病人办卡方式
     */
    private String custom30;

    /**
     * 卡类型
     */
    private String cardType;

    /**
     * 账户锁定
     */
    private String accountLocked;

    /**
     * 账户锁定时间
     */
    private Date accountLockedDate;

    /**
     * 出生时间
     */
    private Date birthTime;

    /**
     * 居住街道
     */
    private String homeStreet;

    /**
     * 户口街道
     */
    private String registeredStreet;

    /**
     * 年龄
     */
    private String age;

    /**
     * 过敏信息
     */
    private String allergyInformation;

    /**
     * 证件号
     */
    private String certificatesNo;

    /**
     * 证件类型
     */
    private String certificatesType;

    /**
     * 血型
     */
    private String blood;

    @Override
    protected Serializable pkVal() {
        return this.personId;
    }


    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getNameTypeCd() {
        return nameTypeCd;
    }

    public void setNameTypeCd(String nameTypeCd) {
        this.nameTypeCd = nameTypeCd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameCode() {
        return nameCode;
    }

    public void setNameCode(String nameCode) {
        this.nameCode = nameCode;
    }

    public String getNameSpellCode() {
        return nameSpellCode;
    }

    public void setNameSpellCode(String nameSpellCode) {
        this.nameSpellCode = nameSpellCode;
    }

    public String getNameWbCode() {
        return nameWbCode;
    }

    public void setNameWbCode(String nameWbCode) {
        this.nameWbCode = nameWbCode;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getBirthProvince() {
        return birthProvince;
    }

    public void setBirthProvince(String birthProvince) {
        this.birthProvince = birthProvince;
    }

    public String getBirthCity() {
        return birthCity;
    }

    public void setBirthCity(String birthCity) {
        this.birthCity = birthCity;
    }

    public String getBirthCounty() {
        return birthCounty;
    }

    public void setBirthCounty(String birthCounty) {
        this.birthCounty = birthCounty;
    }

    public String getBirthZip() {
        return birthZip;
    }

    public void setBirthZip(String birthZip) {
        this.birthZip = birthZip;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getMultipleBirthInd() {
        return multipleBirthInd;
    }

    public void setMultipleBirthInd(String multipleBirthInd) {
        this.multipleBirthInd = multipleBirthInd;
    }

    public String getBirthOrder() {
        return birthOrder;
    }

    public void setBirthOrder(String birthOrder) {
        this.birthOrder = birthOrder;
    }

    public String getMothersMaidenName() {
        return mothersMaidenName;
    }

    public void setMothersMaidenName(String mothersMaidenName) {
        this.mothersMaidenName = mothersMaidenName;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getHealthCard() {
        return healthCard;
    }

    public void setHealthCard(String healthCard) {
        this.healthCard = healthCard;
    }

    public String getCitizenCard() {
        return citizenCard;
    }

    public void setCitizenCard(String citizenCard) {
        this.citizenCard = citizenCard;
    }

    public String getMedicalCertificate() {
        return medicalCertificate;
    }

    public void setMedicalCertificate(String medicalCertificate) {
        this.medicalCertificate = medicalCertificate;
    }

    public String getMeicarePerson() {
        return meicarePerson;
    }

    public void setMeicarePerson(String meicarePerson) {
        this.meicarePerson = meicarePerson;
    }

    public String getElderCertificate() {
        return elderCertificate;
    }

    public void setElderCertificate(String elderCertificate) {
        this.elderCertificate = elderCertificate;
    }

    public String getOpcaseno() {
        return opcaseno;
    }

    public void setOpcaseno(String opcaseno) {
        this.opcaseno = opcaseno;
    }

    public String getInsuranceNo() {
        return insuranceNo;
    }

    public void setInsuranceNo(String insuranceNo) {
        this.insuranceNo = insuranceNo;
    }

    public String getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(String insuranceType) {
        this.insuranceType = insuranceType;
    }

    public String getInsuranceName() {
        return insuranceName;
    }

    public void setInsuranceName(String insuranceName) {
        this.insuranceName = insuranceName;
    }

    public String getGenderCd() {
        return genderCd;
    }

    public void setGenderCd(String genderCd) {
        this.genderCd = genderCd;
    }

    public String getGenderName() {
        return genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    public String getGenderDomain() {
        return genderDomain;
    }

    public void setGenderDomain(String genderDomain) {
        this.genderDomain = genderDomain;
    }

    public String getEthnicGroupCd() {
        return ethnicGroupCd;
    }

    public void setEthnicGroupCd(String ethnicGroupCd) {
        this.ethnicGroupCd = ethnicGroupCd;
    }

    public String getEthnicName() {
        return ethnicName;
    }

    public void setEthnicName(String ethnicName) {
        this.ethnicName = ethnicName;
    }

    public String getEthnicDomain() {
        return ethnicDomain;
    }

    public void setEthnicDomain(String ethnicDomain) {
        this.ethnicDomain = ethnicDomain;
    }

    public String getRaceCd() {
        return raceCd;
    }

    public void setRaceCd(String raceCd) {
        this.raceCd = raceCd;
    }

    public String getRaceName() {
        return raceName;
    }

    public void setRaceName(String raceName) {
        this.raceName = raceName;
    }

    public String getRaceDomain() {
        return raceDomain;
    }

    public void setRaceDomain(String raceDomain) {
        this.raceDomain = raceDomain;
    }

    public String getNationalityCd() {
        return nationalityCd;
    }

    public void setNationalityCd(String nationalityCd) {
        this.nationalityCd = nationalityCd;
    }

    public String getNationalityName() {
        return nationalityName;
    }

    public void setNationalityName(String nationalityName) {
        this.nationalityName = nationalityName;
    }

    public String getNationalityDomain() {
        return nationalityDomain;
    }

    public void setNationalityDomain(String nationalityDomain) {
        this.nationalityDomain = nationalityDomain;
    }

    public String getLanguageCd() {
        return languageCd;
    }

    public void setLanguageCd(String languageCd) {
        this.languageCd = languageCd;
    }

    public String getReligionCd() {
        return religionCd;
    }

    public void setReligionCd(String religionCd) {
        this.religionCd = religionCd;
    }

    public String getMaritalStatusCd() {
        return maritalStatusCd;
    }

    public void setMaritalStatusCd(String maritalStatusCd) {
        this.maritalStatusCd = maritalStatusCd;
    }

    public String getMaritalStatusName() {
        return maritalStatusName;
    }

    public void setMaritalStatusName(String maritalStatusName) {
        this.maritalStatusName = maritalStatusName;
    }

    public String getMaritalDomain() {
        return maritalDomain;
    }

    public void setMaritalDomain(String maritalDomain) {
        this.maritalDomain = maritalDomain;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public String getDegreeDomain() {
        return degreeDomain;
    }

    public void setDegreeDomain(String degreeDomain) {
        this.degreeDomain = degreeDomain;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHomeProvince() {
        return homeProvince;
    }

    public void setHomeProvince(String homeProvince) {
        this.homeProvince = homeProvince;
    }

    public String getHomeCity() {
        return homeCity;
    }

    public void setHomeCity(String homeCity) {
        this.homeCity = homeCity;
    }

    public String getHomeCounty() {
        return homeCounty;
    }

    public void setHomeCounty(String homeCounty) {
        this.homeCounty = homeCounty;
    }

    public String getHomeZip() {
        return homeZip;
    }

    public void setHomeZip(String homeZip) {
        this.homeZip = homeZip;
    }

    public String getHomeAddress() {
        if (StringUtils.isEmpty(homeAddress)) {
            if (!StringUtils.isEmpty(getRegisteredAddress())) {
                this.homeAddress = this.registeredAddress;
            }
        }
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getRegisteredProvince() {
        return registeredProvince;
    }

    public void setRegisteredProvince(String registeredProvince) {
        this.registeredProvince = registeredProvince;
    }

    public String getRegisteredCity() {
        return registeredCity;
    }

    public void setRegisteredCity(String registeredCity) {
        this.registeredCity = registeredCity;
    }

    public String getRegisteredCounty() {
        return registeredCounty;
    }

    public void setRegisteredCounty(String registeredCounty) {
        this.registeredCounty = registeredCounty;
    }

    public String getRegisteredZip() {
        return registeredZip;
    }

    public void setRegisteredZip(String registeredZip) {
        this.registeredZip = registeredZip;
    }

    public String getRegisteredAddress() {
        return registeredAddress;
    }

    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    public String getNativeProvince() {
        return nativeProvince;
    }

    public void setNativeProvince(String nativeProvince) {
        this.nativeProvince = nativeProvince;
    }

    public String getNativeCity() {
        return nativeCity;
    }

    public void setNativeCity(String nativeCity) {
        this.nativeCity = nativeCity;
    }

    public String getWorkZip() {
        return workZip;
    }

    public void setWorkZip(String workZip) {
        this.workZip = workZip;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddressTypeCd() {
        return addressTypeCd;
    }

    public void setAddressTypeCd(String addressTypeCd) {
        this.addressTypeCd = addressTypeCd;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getPostalCode1() {
        return postalCode1;
    }

    public void setPostalCode1(String postalCode1) {
        this.postalCode1 = postalCode1;
    }

    public String getAddress1TypeCd() {
        return address1TypeCd;
    }

    public void setAddress1TypeCd(String address1TypeCd) {
        this.address1TypeCd = address1TypeCd;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneCountryCode() {
        return phoneCountryCode;
    }

    public void setPhoneCountryCode(String phoneCountryCode) {
        this.phoneCountryCode = phoneCountryCode;
    }

    public String getPhoneAreaCode() {
        return phoneAreaCode;
    }

    public void setPhoneAreaCode(String phoneAreaCode) {
        this.phoneAreaCode = phoneAreaCode;
    }

    public String getPhoneNumber() {
        if (StringUtils.isEmpty(phoneNumber)) {
            if (!StringUtils.isEmpty(getHomeNumber())) {
                this.phoneNumber = this.homeNumber;
            }
        }
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneExt() {
        return phoneExt;
    }

    public void setPhoneExt(String phoneExt) {
        this.phoneExt = phoneExt;
    }

    public String getPhoneTypeCd() {
        return phoneTypeCd;
    }

    public void setPhoneTypeCd(String phoneTypeCd) {
        this.phoneTypeCd = phoneTypeCd;
    }

    public String getPhoneCountryCode1() {
        return phoneCountryCode1;
    }

    public void setPhoneCountryCode1(String phoneCountryCode1) {
        this.phoneCountryCode1 = phoneCountryCode1;
    }

    public String getPhoneAreaCode1() {
        return phoneAreaCode1;
    }

    public void setPhoneAreaCode1(String phoneAreaCode1) {
        this.phoneAreaCode1 = phoneAreaCode1;
    }

    public String getPhoneNumber1() {
        return phoneNumber1;
    }

    public void setPhoneNumber1(String phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }

    public String getPhoneExt1() {
        return phoneExt1;
    }

    public void setPhoneExt1(String phoneExt1) {
        this.phoneExt1 = phoneExt1;
    }

    public String getPhoneTypeCd1() {
        return phoneTypeCd1;
    }

    public void setPhoneTypeCd1(String phoneTypeCd1) {
        this.phoneTypeCd1 = phoneTypeCd1;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanycontacts() {
        return companycontacts;
    }

    public void setCompanycontacts(String companycontacts) {
        this.companycontacts = companycontacts;
    }

    public String getBirthplaceCd() {
        return birthplaceCd;
    }

    public void setBirthplaceCd(String birthplaceCd) {
        this.birthplaceCd = birthplaceCd;
    }

    public String getWorkstatus() {
        return workstatus;
    }

    public void setWorkstatus(String workstatus) {
        this.workstatus = workstatus;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getProfessionName() {
        return professionName;
    }

    public void setProfessionName(String professionName) {
        this.professionName = professionName;
    }

    public String getProfessionDomain() {
        return professionDomain;
    }

    public void setProfessionDomain(String professionDomain) {
        this.professionDomain = professionDomain;
    }

    public String getPrivateNumber() {
        return privateNumber;
    }

    public void setPrivateNumber(String privateNumber) {
        this.privateNumber = privateNumber;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber;
    }

    public String getGuardianPerson() {
        return guardianPerson;
    }

    public void setGuardianPerson(String guardianPerson) {
        this.guardianPerson = guardianPerson;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public String getAccountIdentifierDomainId() {
        return accountIdentifierDomainId;
    }

    public void setAccountIdentifierDomainId(String accountIdentifierDomainId) {
        this.accountIdentifierDomainId = accountIdentifierDomainId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getDeathInd() {
        return deathInd;
    }

    public void setDeathInd(String deathInd) {
        this.deathInd = deathInd;
    }

    public Date getDeathTime() {
        return deathTime;
    }

    public void setDeathTime(Date deathTime) {
        this.deathTime = deathTime;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public Date getDateChanged() {
        return dateChanged;
    }

    public void setDateChanged(Date dateChanged) {
        this.dateChanged = dateChanged;
    }

    public String getChangedById() {
        return changedById;
    }

    public void setChangedById(String changedById) {
        this.changedById = changedById;
    }

    public Date getDateVoided() {
        return dateVoided;
    }

    public void setDateVoided(Date dateVoided) {
        this.dateVoided = dateVoided;
    }

    public String getVoidedById() {
        return voidedById;
    }

    public void setVoidedById(String voidedById) {
        this.voidedById = voidedById;
    }

    public String getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(String groupNumber) {
        this.groupNumber = groupNumber;
    }

    public String getBloodTypeCd() {
        return bloodTypeCd;
    }

    public void setBloodTypeCd(String bloodTypeCd) {
        this.bloodTypeCd = bloodTypeCd;
    }

    public String getRhType() {
        return rhType;
    }

    public void setRhType(String rhType) {
        this.rhType = rhType;
    }

    public String getEmpi() {
        return empi;
    }

    public void setEmpi(String empi) {
        this.empi = empi;
    }

    public String getHospitalCd() {
        return hospitalCd;
    }

    public void setHospitalCd(String hospitalCd) {
        this.hospitalCd = hospitalCd;
    }

    public String getCustom1() {
        return custom1;
    }

    public void setCustom1(String custom1) {
        this.custom1 = custom1;
    }

    public String getCustom2() {
        return custom2;
    }

    public void setCustom2(String custom2) {
        this.custom2 = custom2;
    }

    public String getCustom3() {
        return custom3;
    }

    public void setCustom3(String custom3) {
        this.custom3 = custom3;
    }

    public String getCustom4() {
        return custom4;
    }

    public void setCustom4(String custom4) {
        this.custom4 = custom4;
    }

    public String getCustom5() {
        return custom5;
    }

    public void setCustom5(String custom5) {
        this.custom5 = custom5;
    }

    public String getCustom6() {
        return custom6;
    }

    public void setCustom6(String custom6) {
        this.custom6 = custom6;
    }

    public String getCustom7() {
        return custom7;
    }

    public void setCustom7(String custom7) {
        this.custom7 = custom7;
    }

    public String getCustom8() {
        return custom8;
    }

    public void setCustom8(String custom8) {
        this.custom8 = custom8;
    }

    public String getCustom9() {
        return custom9;
    }

    public void setCustom9(String custom9) {
        this.custom9 = custom9;
    }

    public String getCustom10() {
        return custom10;
    }

    public void setCustom10(String custom10) {
        this.custom10 = custom10;
    }

    public String getCustom11() {
        return custom11;
    }

    public void setCustom11(String custom11) {
        this.custom11 = custom11;
    }

    public String getCustom12() {
        return custom12;
    }

    public void setCustom12(String custom12) {
        this.custom12 = custom12;
    }

    public String getCustom13() {
        return custom13;
    }

    public void setCustom13(String custom13) {
        this.custom13 = custom13;
    }

    public String getCustom14() {
        return custom14;
    }

    public void setCustom14(String custom14) {
        this.custom14 = custom14;
    }

    public String getCustom15() {
        if (StringUtils.isEmpty(custom15)) {
            this.custom15 = "";
        }
        return custom15;
    }

    public void setCustom15(String custom15) {
        this.custom15 = custom15;
    }

    public String getCustom16() {
        return custom16;
    }

    public void setCustom16(String custom16) {
        this.custom16 = custom16;
    }

    public String getCustom17() {
        return custom17;
    }

    public void setCustom17(String custom17) {
        this.custom17 = custom17;
    }

    public String getCustom18() {
        return custom18;
    }

    public void setCustom18(String custom18) {
        this.custom18 = custom18;
    }

    public String getCustom19() {
        return custom19;
    }

    public void setCustom19(String custom19) {
        this.custom19 = custom19;
    }

    public String getCustom20() {
        return custom20;
    }

    public void setCustom20(String custom20) {
        this.custom20 = custom20;
    }

    public String getCustom21() {
        return custom21;
    }

    public void setCustom21(String custom21) {
        this.custom21 = custom21;
    }

    public String getCustom22() {
        return custom22;
    }

    public void setCustom22(String custom22) {
        this.custom22 = custom22;
    }

    public String getCustom23() {
        return custom23;
    }

    public void setCustom23(String custom23) {
        this.custom23 = custom23;
    }

    public String getCustom24() {
        return custom24;
    }

    public void setCustom24(String custom24) {
        this.custom24 = custom24;
    }

    public String getCustom25() {
        return custom25;
    }

    public void setCustom25(String custom25) {
        this.custom25 = custom25;
    }

    public String getCustom26() {
        return custom26;
    }

    public void setCustom26(String custom26) {
        this.custom26 = custom26;
    }

    public String getCustom27() {
        return custom27;
    }

    public void setCustom27(String custom27) {
        this.custom27 = custom27;
    }

    public String getCustom28() {
        return custom28;
    }

    public void setCustom28(String custom28) {
        this.custom28 = custom28;
    }

    public String getCustom29() {
        return custom29;
    }

    public void setCustom29(String custom29) {
        this.custom29 = custom29;
    }

    public String getCustom30() {
        return custom30;
    }

    public void setCustom30(String custom30) {
        this.custom30 = custom30;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getAccountLocked() {
        return accountLocked;
    }

    public void setAccountLocked(String accountLocked) {
        this.accountLocked = accountLocked;
    }

    public Date getAccountLockedDate() {
        return accountLockedDate;
    }

    public void setAccountLockedDate(Date accountLockedDate) {
        this.accountLockedDate = accountLockedDate;
    }

    public Date getBirthTime() {
        return birthTime;
    }

    public void setBirthTime(Date birthTime) {
        this.birthTime = birthTime;
    }

    public String getHomeStreet() {
        return homeStreet;
    }

    public void setHomeStreet(String homeStreet) {
        this.homeStreet = homeStreet;
    }

    public String getRegisteredStreet() {
        return registeredStreet;
    }

    public void setRegisteredStreet(String registeredStreet) {
        this.registeredStreet = registeredStreet;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAllergyInformation() {
        return allergyInformation;
    }

    public void setAllergyInformation(String allergyInformation) {
        this.allergyInformation = allergyInformation;
    }

    public String getCertificatesNo() {
        return certificatesNo;
    }

    public void setCertificatesNo(String certificatesNo) {
        this.certificatesNo = certificatesNo;
    }

    public String getCertificatesType() {
        if (StringUtils.isEmpty(certificatesType)) {
            this.certificatesNo = ID_NUMBER_DEFAULT_CODE;
        }
        return certificatesType;
    }

    public void setCertificatesType(String certificatesType) {
        this.certificatesType = certificatesType;
    }

    public String getBlood() {
        return blood;
    }

    public void setBlood(String blood) {
        this.blood = blood;
    }

    @Override
    public String toString() {
        return "Person{" +
                "personId=" + personId +
                ", givenName=" + givenName +
                ", middleName=" + middleName +
                ", familyName=" + familyName +
                ", prefix=" + prefix +
                ", suffix=" + suffix +
                ", nameTypeCd=" + nameTypeCd +
                ", name=" + name +
                ", nameCode=" + nameCode +
                ", nameSpellCode=" + nameSpellCode +
                ", nameWbCode=" + nameWbCode +
                ", dateOfBirth=" + dateOfBirth +
                ", birthProvince=" + birthProvince +
                ", birthCity=" + birthCity +
                ", birthCounty=" + birthCounty +
                ", birthZip=" + birthZip +
                ", birthPlace=" + birthPlace +
                ", multipleBirthInd=" + multipleBirthInd +
                ", birthOrder=" + birthOrder +
                ", mothersMaidenName=" + mothersMaidenName +
                ", ssn=" + ssn +
                ", identityNo=" + identityNo +
                ", healthCard=" + healthCard +
                ", citizenCard=" + citizenCard +
                ", medicalCertificate=" + medicalCertificate +
                ", meicarePerson=" + meicarePerson +
                ", elderCertificate=" + elderCertificate +
                ", opcaseno=" + opcaseno +
                ", insuranceNo=" + insuranceNo +
                ", insuranceType=" + insuranceType +
                ", insuranceName=" + insuranceName +
                ", genderCd=" + genderCd +
                ", genderName=" + genderName +
                ", genderDomain=" + genderDomain +
                ", ethnicGroupCd=" + ethnicGroupCd +
                ", ethnicName=" + ethnicName +
                ", ethnicDomain=" + ethnicDomain +
                ", raceCd=" + raceCd +
                ", raceName=" + raceName +
                ", raceDomain=" + raceDomain +
                ", nationalityCd=" + nationalityCd +
                ", nationalityName=" + nationalityName +
                ", nationalityDomain=" + nationalityDomain +
                ", languageCd=" + languageCd +
                ", religionCd=" + religionCd +
                ", maritalStatusCd=" + maritalStatusCd +
                ", maritalStatusName=" + maritalStatusName +
                ", maritalDomain=" + maritalDomain +
                ", degree=" + degree +
                ", degreeName=" + degreeName +
                ", degreeDomain=" + degreeDomain +
                ", email=" + email +
                ", homeProvince=" + homeProvince +
                ", homeCity=" + homeCity +
                ", homeCounty=" + homeCounty +
                ", homeZip=" + homeZip +
                ", homeAddress=" + homeAddress +
                ", registeredProvince=" + registeredProvince +
                ", registeredCity=" + registeredCity +
                ", registeredCounty=" + registeredCounty +
                ", registeredZip=" + registeredZip +
                ", registeredAddress=" + registeredAddress +
                ", nativeProvince=" + nativeProvince +
                ", nativeCity=" + nativeCity +
                ", workZip=" + workZip +
                ", workAddress=" + workAddress +
                ", address1=" + address1 +
                ", postalCode=" + postalCode +
                ", addressTypeCd=" + addressTypeCd +
                ", address2=" + address2 +
                ", postalCode1=" + postalCode1 +
                ", address1TypeCd=" + address1TypeCd +
                ", city=" + city +
                ", state=" + state +
                ", country=" + country +
                ", countryCode=" + countryCode +
                ", phoneCountryCode=" + phoneCountryCode +
                ", phoneAreaCode=" + phoneAreaCode +
                ", phoneNumber=" + phoneNumber +
                ", phoneExt=" + phoneExt +
                ", phoneTypeCd=" + phoneTypeCd +
                ", phoneCountryCode1=" + phoneCountryCode1 +
                ", phoneAreaCode1=" + phoneAreaCode1 +
                ", phoneNumber1=" + phoneNumber1 +
                ", phoneExt1=" + phoneExt1 +
                ", phoneTypeCd1=" + phoneTypeCd1 +
                ", company=" + company +
                ", companycontacts=" + companycontacts +
                ", birthplaceCd=" + birthplaceCd +
                ", workstatus=" + workstatus +
                ", profession=" + profession +
                ", professionName=" + professionName +
                ", professionDomain=" + professionDomain +
                ", privateNumber=" + privateNumber +
                ", homeNumber=" + homeNumber +
                ", workNumber=" + workNumber +
                ", guardianPerson=" + guardianPerson +
                ", vip=" + vip +
                ", accountIdentifierDomainId=" + accountIdentifierDomainId +
                ", account=" + account +
                ", deathInd=" + deathInd +
                ", deathTime=" + deathTime +
                ", dateCreated=" + dateCreated +
                ", creatorId=" + creatorId +
                ", dateChanged=" + dateChanged +
                ", changedById=" + changedById +
                ", dateVoided=" + dateVoided +
                ", voidedById=" + voidedById +
                ", groupNumber=" + groupNumber +
                ", bloodTypeCd=" + bloodTypeCd +
                ", rhType=" + rhType +
                ", empi=" + empi +
                ", hospitalCd=" + hospitalCd +
                ", custom1=" + custom1 +
                ", custom2=" + custom2 +
                ", custom3=" + custom3 +
                ", custom4=" + custom4 +
                ", custom5=" + custom5 +
                ", custom6=" + custom6 +
                ", custom7=" + custom7 +
                ", custom8=" + custom8 +
                ", custom9=" + custom9 +
                ", custom10=" + custom10 +
                ", custom11=" + custom11 +
                ", custom12=" + custom12 +
                ", custom13=" + custom13 +
                ", custom14=" + custom14 +
                ", custom15=" + custom15 +
                ", custom16=" + custom16 +
                ", custom17=" + custom17 +
                ", custom18=" + custom18 +
                ", custom19=" + custom19 +
                ", custom20=" + custom20 +
                ", custom21=" + custom21 +
                ", custom22=" + custom22 +
                ", custom23=" + custom23 +
                ", custom24=" + custom24 +
                ", custom25=" + custom25 +
                ", custom26=" + custom26 +
                ", custom27=" + custom27 +
                ", custom28=" + custom28 +
                ", custom29=" + custom29 +
                ", custom30=" + custom30 +
                ", cardType=" + cardType +
                ", accountLocked=" + accountLocked +
                ", accountLockedDate=" + accountLockedDate +
                ", birthTime=" + birthTime +
                ", homeStreet=" + homeStreet +
                ", registeredStreet=" + registeredStreet +
                ", age=" + age +
                ", allergyInformation=" + allergyInformation +
                ", certificatesNo=" + certificatesNo +
                ", certificatesType=" + certificatesType +
                ", blood=" + blood +
                "}";
    }

}
