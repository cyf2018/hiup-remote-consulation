package com.hiup.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 病人就诊信息
 * </p>
 *
 * @author cyf
 * @since 2018-12-19
 */
@TableName("PATIENT_VISIT")
public class PatientVisit extends Model<PatientVisit> {

    private static final long serialVersionUID = 1L;

    /**
     * 就诊类型--必填 1:门诊,2:住院,3:急诊,4:其他 默认为其它
     */
    public static final String DEFAULT_INHOS_TYPE = "4";

    /**
     * 就诊自增长
     */
    @TableId(value="visit_id",type= IdType.AUTO)
    private String visitId;

    /**
     * 患者ID
     */
    private String personId;

    /**
     * 患者域ID
     */
    private String personDomain;

    /**
     * 流水ID
     */
    private String visitFlowId;

    /**
     * 流水域ID
     */
    private String visitFlowDomain;

    /**
     * 医院域ID
     */
    private String hospitalDomain;

    /**
     * 患者类别
     */
    private String patCategory;

    /**
     * 入院护理区域
     */
    private String patCurrentPointOfCare;

    /**
     * 入院病区
     */
    private String patCurrentRoom;

    /**
     * 入院床位
     */
    private String patCurrentBed;

    /**
     * 入院科室
     */
    private String patCuurentDep;

    /**
     * 入院位置的状态
     */
    private String patCurrentPositionStatus;

    /**
     * 入院位置的类型
     */
    private String patCurrentPositionType;

    /**
     * 入院大楼
     */
    private String patCurrentBuilding;

    /**
     * 入院楼层
     */
    private String patCurrentFloor;

    /**
     * 入院相关描述
     */
    private String patCuurentDescription;

    /**
     * 允许入院类型
     */
    private String patAdmissionType;

    /**
     * 允许入院号码
     */
    private String patAdmissionNumber;

    /**
     * 病人前护理区域
     */
    private String patFormerPointOfCare;

    /**
     * 病人前病区
     */
    private String patFormerRoom;

    /**
     * 病人前床位
     */
    private String patFormerBed;

    /**
     * 病人前科室
     */
    private String patFormerDep;

    /**
     * 病人前位置状态
     */
    private String patFormerPositionStatus;

    /**
     * 病人前位置类型
     */
    private String patFormerPositionType;

    /**
     * 病人前大楼
     */
    private String patFormerBuilding;

    /**
     * 病人前楼层
     */
    private String patFormerFloor;

    /**
     * 病人前相关描述
     */
    private String patFormerDescription;

    /**
     * 住院医师
     */
    private String admissionsDoctor;

    /**
     * 住院医师ID
     */
    private String admissionsDoctorId;

    /**
     * 主治医师
     */
    private String referringDoctor;

    /**
     * 主治医师ID
     */
    private String referringDoctorId;

    /**
     * 主任医师
     */
    private String consultationDoctor;

    /**
     * 主任医师ID
     */
    private String consultationDoctorId;

    /**
     * 医院服务
     */
    private String hospitalService;

    /**
     * 临时护理区域
     */
    private String patTempPointOfCare;

    /**
     * 临时病区
     */
    private String patTempRoom;

    /**
     * 临时床位
     */
    private String patTempBed;

    /**
     * 临时科室
     */
    private String patTempDep;

    /**
     * 临时位置状态
     */
    private String patTempPositionStatus;

    /**
     * 临时位置类型
     */
    private String patTempPositionType;

    /**
     * 临时大楼
     */
    private String patTempBuilding;

    /**
     * 临时楼层
     */
    private String patTempFloor;

    /**
     * 临时相关描述
     */
    private String patTempDescription;

    /**
     * 实验
     */
    private String patAdmissionTest;

    /**
     * 住院次数
     */
    private String patReAdmission;

    /**
     * 入院来源编码
     */
    private String patAdmissionSource;

    /**
     * 走动状态
     */
    private String patAmbulatoryStatus;

    /**
     * VIP标志
     */
    private String patVip;

    /**
     * 门诊接诊医生
     */
    private String patAdmissionDoctors;

    /**
     * 门诊接诊医生ID
     */
    private String patAdmissionDoctorsId;

    /**
     * 患者类型
     */
    private String patientClass;

    /**
     * 病人ID
     */
    private String patientId;

    /**
     * 财务类
     */
    private String patFinancialClass;

    /**
     * 床位价格
     */
    private String roomBedCostPrice;

    /**
     * 住院状态
     */
    private String courtesyCode;

    /**
     * 病例分型
     */
    private String creditRating;

    /**
     * 合同编码
     */
    private String contractCode;

    /**
     * 合同创建日期
     */
    private String contractCreateDate;

    /**
     * 合同价格
     */
    private String contractPrice;

    /**
     * 合同时间
     */
    private String contractTime;

    /**
     * 支付比例编码
     */
    private String interestRateCode;

    /**
     * 坏账
     */
    private String badDebts;

    /**
     * 坏账创建日期
     */
    private String badDebtsCreateDate;

    /**
     * 坏账编码
     */
    private String badDebtsCode;

    /**
     * 坏账费用
     */
    private String badDebtsPrice;

    /**
     * 坏账恢复
     */
    //private String badDebtsRestorePrice;

    /**
     * 账户无效
     */
    private String patAccountVoided;

    /**
     * 账户无效日期
     */
    private String patAccountVoidedDate;

    /**
     * 出院处置编码
     */
    private String patDischargeDisposition;

    /**
     * 出院位置
     */
    private String patDischargeLocation;

    /**
     * 饮食类型
     */
    private String patDietType;

    /**
     * 服务机构
     */
    private String patServiceAgencies;

    /**
     * 床位标志
     */
    private String patBedStatus;

    /**
     * 支付方式编码
     */
    private String patAccountStatus;

    /**
     * 取消的护理区域
     */
    private String patDeterPointOfCare;

    /**
     * 取消的病区
     */
    private String patDeterRoom;

    /**
     * 取消的床位
     */
    private String patDeterBed;

    /**
     * 取消时科室
     */
    private String patDeterDep;

    /**
     * 取消时位置状态
     */
    private String patDeterPositionStatus;

    /**
     * 取消时位置类型
     */
    private String patDeterPositionType;

    /**
     * 取消时大楼
     */
    private String patDeterBuilding;

    /**
     * 取消时楼层
     */
    private String patDeterFloor;

    /**
     * 取消时相关描述
     */
    private String patDeterDescription;

    /**
     * 前临时护理区域
     */
    private String patForTempPointOfCare;

    /**
     * 前临时病区
     */
    private String patForTempRoom;

    /**
     * 前临时床位
     */
    private String patForTempBed;

    /**
     * 前临时科室
     */
    private String patForTempDep;

    /**
     * 前临时位置状态
     */
    private String patForTempPositionStatus;

    /**
     * 前临时位置类型
     */
    private String patForTempPositionType;

    /**
     * 前临时大楼
     */
    private String patForTempBuilding;

    /**
     * 前临时楼层
     */
    private String patForTempFloor;

    /**
     * 前临时相关描述
     */
    private String patForTempDescription;

    /**
     * 入院日期
     */
    private Date admitDate;

    /**
     * 出院日期
     */
    private Date dischargeDate;

    /**
     * 差额
     */
    private String patDifference;

    /**
     * 总价
     */
    private String patTotalCost;

    /**
     * 总调度
     */
    private String patTotalDispatch;

    /**
     * 应付的总额
     */
    private String patTotalAmountPayable;

    /**
     * 多余的ID
     */
    private String patSpareId;

    /**
     * 访问标志
     */
    private String patVisitLogo;

    /**
     * 其他卫生机构
     */
    private String otherMedicalInstitutions;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 创建者
     */
    private String createId;

    /**
     * 废弃日
     */
    private Date voidedDate;

    /**
     * 废弃标识
     */
    private String voidedId;

    /**
     * 修改日
     */
    private Date modifyDate;

    /**
     * 修改者
     */
    private String modifyId;

    /**
     * 自定义1
     */
    private String custom1;

    /**
     * 自定义2
     */
    private String custom2;

    /**
     * 自定义3
     */
    private String custom3;

    /**
     * 自定义4
     */
    private String custom4;

    /**
     * 自定义5
     */
    private String custom5;

    /**
     * 护士ID
     */
    private String patNurseCode;

    /**
     * 护士姓名
     */
    private String patNurseName;

    /**
     * 护理
     */
    private String tend;

    /**
     * 婴儿标志
     */
    private String babyFlag;

    /**
     * 入院体重
     */
    private String admitWeight;

    /**
     * 出生体重
     */
    private String birthWeight;

    /**
     * 入院登记操作员
     */
    private String operCode;

    /**
     * 操作日期
     */
    private Date operDate;

    /**
     * 前缀
     */
    private String prefix;

    /**
     * 保险类型
     */
    private String insuranceType;

    /**
     * 联系人
     */
    private String contactPerson;

    /**
     * 联系关系
     */
    private String contactRelations;

    /**
     * 联系地址
     */
    private String contactAddress;

    /**
     * 联系电话
     */
    private String contactPhone;

    /**
     * 患者类型名称
     */
    private String patCategoryName;

    /**
     * 性别名称
     */
    private String genderName;

    /**
     * 付费标准
     */
    private String payRate;

    /**
     * 离院处置名称
     */
    private String dischargeName;

    /**
     * 保险名称
     */
    private String insuranceName;

    /**
     * 入院时情况名称
     */
    private String admissionName;

    /**
     * 病人住院状态名称
     */
    private String ipStatusName;

    /**
     * 病例分型名称
     */
    private String dificultyName;

    /**
     * 许可进入名称
     */
    private String admitWayName;

    /**
     * 入院体重单位
     */
    private String admitWeightUnit;

    /**
     * 出院体重单位
     */
    private String babyWeightUnit;

    /**
     * 药费限额
     */
    private String medicinelimitamount;

    /**
     * 床位限额
     */
    private String sickbedlimitamount;

    /**
     * 检查限额
     */
    private String examinelimitamount;

    /**
     * 治疗限额
     */
    private String curelimitamount;

    /**
     * HIUP状态
     */
    private String hiupStatus;

    /**
     * HIUP错误信息
     */
    private String hiupErrorInfo;

    /**
     * 入院时情况编码系统
     */
    private String admissionDomain;

    /**
     * 入院途径编码系统
     */
    private String admissionSourceDomain;

    /**
     * 入院途径名称
     */
    private String admissionSourceName;

    /**
     * 患者类型编码
     */
    private String patientClassName;

    /**
     * 患者类型编码系统
     */
    private String patientClassDomain;

    /**
     * 病人住院状态编码系统
     */
    private String ipStatusDomain;

    /**
     * 病例分型编码系统
     */
    private String dificultyDomain;

    /**
     * 离院处置编码系统
     */
    private String dischargeDomain;

    /**
     * 支付方式名称
     */
    private String accountStatusName;

    /**
     * 支付方式编码系统
     */
    private String accountStatusDomain;

    /**
     * 性别域
     */
    private String genderDomain;

    /**
     * 患者类别编码系统
     */
    private String patCategorySystem;

    /**
     * 母亲ID
     */
    private String mothersId;

    /**
     * 母亲域
     */
    private String mothersDomain;

    /**
     * 母亲流水ID
     */
    private String mothersFlowId;

    /**
     * 母亲流水域
     */
    private String mothersFlowDomain;

    /**
     * 母亲姓名
     */
    private String mothersName;

    /**
     * 后续编码
     */
    private String noonCode;

    /**
     * 支付编码
     */
    private String paykindCode;

    /**
     * 支付名称
     */
    private String paykindName;

    /**
     * 计划、图解、概要号
     */
    private String schemaNo;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 看编号
     */
    private String seeno;

    /**
     * 开始时间
     */
    private Date beginTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 年鉴
     */
    private String ynbook;

    /**
     * 年鉴故障率
     */
    private String ynfr;

    /**
     * 附加标记
     */
    private String appendFlag;

    private String ynsee;

    /**
     * 看日期
     */
    private Date seeDate;

    /**
     * 诊断标志
     */
    private String triageFlag;

    /**
     * 诊断OP编号
     */
    private String triageOpcd;

    /**
     * 诊断日期
     */
    private Date triageDate;

    /**
     * 看DP编号
     */
    private String seeDpcd;

    /**
     * 看DO编号
     */
    private String seeDocd;

    /**
     * OUT患者状态A
     */
    private String outPatientStatusA;

    /**
     * OUT患者状态B
     */
    private String outPatientStatusB;

    /**
     * OUT患者状态C
     */
    private String outPatientStatusC;

    /**
     * IN患者状态A
     */
    private String inPatientStatusA;

    /**
     * IN患者状态B
     */
    private String inPatientStatusB;

    /**
     * IN患者状态C
     */
    private String inPatientStatusC;

    /**
     * 患者来源名称
     */
    private String patientSourceName;

    /**
     * 旧患者ID
     */
    private String oldPatientId;

    /**
     * 旧患者域ID
     */
    private String oldPatientDomain;

    /**
     * 旧患者流水ID
     */
    private String oldVisitFlowId;

    /**
     * 旧患者流水域ID
     */
    private String oldVisitFlowDomain;

    /**
     * 旧患者就诊ID
     */
    private String oldVisitId;

    /**
     * 旧person_id
     */
    private String oldPersonId;

    /**
     * 旧状态
     */
    private String oldStatus;

    /**
     * 旧信息
     */
    private String oldInfo;

    /**
     * 紧急情况标识
     */
    private String isemergency;

    /**
     * 次数
     */
    private String patIpTimes;

    /**
     * 诊断ICD
     */
    private String diagnoseIcd;

    /**
     * 诊断名称
     */
    private String diagnoseName;

    /**
     * 自定义6
     */
    private String custom6;

    /**
     * 关联ID
     */
    private String relevanceId;

    /**
     * 关联域
     */
    private String relevanceDomain;

    /**
     * 关联名
     */
    private String relevanceName;

    /**
     * 诊疗组
     */
    private String diagnosisTreatmentGroup;

    /**
     * 诊疗组CODE
     */
    private String diagnosisTreatmentGroupcode;

    /**
     * 护理组
     */
    private String nursingGroup;

    /**
     * 护理组CODE
     */
    private String nursingGroupCode;

    /**
     * 住院目的
     */
    private String hospitalizationPurposes;

    /**
     * 管床医生
     */
    private String tubeBedDoctor;

    /**
     * 操作员姓名
     */
    private String operName;

    /**
     * 首次入科时间
     */
    private String enterDeptTime;

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }
    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }
    public String getPersonDomain() {
        return personDomain;
    }

    public void setPersonDomain(String personDomain) {
        this.personDomain = personDomain;
    }
    public String getVisitFlowId() {
        return visitFlowId;
    }

    public void setVisitFlowId(String visitFlowId) {
        this.visitFlowId = visitFlowId;
    }
    public String getVisitFlowDomain() {
        return visitFlowDomain;
    }

    public void setVisitFlowDomain(String visitFlowDomain) {
        this.visitFlowDomain = visitFlowDomain;
    }
    public String getHospitalDomain() {
        return hospitalDomain;
    }

    public void setHospitalDomain(String hospitalDomain) {
        this.hospitalDomain = hospitalDomain;
    }
    public String getPatCategory() {
        return patCategory;
    }

    public void setPatCategory(String patCategory) {
        this.patCategory = patCategory;
    }
    public String getPatCurrentPointOfCare() {
        return patCurrentPointOfCare;
    }

    public void setPatCurrentPointOfCare(String patCurrentPointOfCare) {
        this.patCurrentPointOfCare = patCurrentPointOfCare;
    }
    public String getPatCurrentRoom() {
        return patCurrentRoom;
    }

    public void setPatCurrentRoom(String patCurrentRoom) {
        this.patCurrentRoom = patCurrentRoom;
    }
    public String getPatCurrentBed() {
        return patCurrentBed;
    }

    public void setPatCurrentBed(String patCurrentBed) {
        this.patCurrentBed = patCurrentBed;
    }
    public String getPatCuurentDep() {
        return patCuurentDep;
    }

    public void setPatCuurentDep(String patCuurentDep) {
        this.patCuurentDep = patCuurentDep;
    }
    public String getPatCurrentPositionStatus() {
        return patCurrentPositionStatus;
    }

    public void setPatCurrentPositionStatus(String patCurrentPositionStatus) {
        this.patCurrentPositionStatus = patCurrentPositionStatus;
    }
    public String getPatCurrentPositionType() {
        return patCurrentPositionType;
    }

    public void setPatCurrentPositionType(String patCurrentPositionType) {
        this.patCurrentPositionType = patCurrentPositionType;
    }
    public String getPatCurrentBuilding() {
        return patCurrentBuilding;
    }

    public void setPatCurrentBuilding(String patCurrentBuilding) {
        this.patCurrentBuilding = patCurrentBuilding;
    }
    public String getPatCurrentFloor() {
        return patCurrentFloor;
    }

    public void setPatCurrentFloor(String patCurrentFloor) {
        this.patCurrentFloor = patCurrentFloor;
    }
    public String getPatCuurentDescription() {
        return patCuurentDescription;
    }

    public void setPatCuurentDescription(String patCuurentDescription) {
        this.patCuurentDescription = patCuurentDescription;
    }
    public String getPatAdmissionType() {
        return patAdmissionType;
    }

    public void setPatAdmissionType(String patAdmissionType) {
        this.patAdmissionType = patAdmissionType;
    }
    public String getPatAdmissionNumber() {
        return patAdmissionNumber;
    }

    public void setPatAdmissionNumber(String patAdmissionNumber) {
        this.patAdmissionNumber = patAdmissionNumber;
    }
    public String getPatFormerPointOfCare() {
        return patFormerPointOfCare;
    }

    public void setPatFormerPointOfCare(String patFormerPointOfCare) {
        this.patFormerPointOfCare = patFormerPointOfCare;
    }
    public String getPatFormerRoom() {
        return patFormerRoom;
    }

    public void setPatFormerRoom(String patFormerRoom) {
        this.patFormerRoom = patFormerRoom;
    }
    public String getPatFormerBed() {
        return patFormerBed;
    }

    public void setPatFormerBed(String patFormerBed) {
        this.patFormerBed = patFormerBed;
    }
    public String getPatFormerDep() {
        return patFormerDep;
    }

    public void setPatFormerDep(String patFormerDep) {
        this.patFormerDep = patFormerDep;
    }
    public String getPatFormerPositionStatus() {
        return patFormerPositionStatus;
    }

    public void setPatFormerPositionStatus(String patFormerPositionStatus) {
        this.patFormerPositionStatus = patFormerPositionStatus;
    }
    public String getPatFormerPositionType() {
        return patFormerPositionType;
    }

    public void setPatFormerPositionType(String patFormerPositionType) {
        this.patFormerPositionType = patFormerPositionType;
    }
    public String getPatFormerBuilding() {
        return patFormerBuilding;
    }

    public void setPatFormerBuilding(String patFormerBuilding) {
        this.patFormerBuilding = patFormerBuilding;
    }
    public String getPatFormerFloor() {
        return patFormerFloor;
    }

    public void setPatFormerFloor(String patFormerFloor) {
        this.patFormerFloor = patFormerFloor;
    }
    public String getPatFormerDescription() {
        return patFormerDescription;
    }

    public void setPatFormerDescription(String patFormerDescription) {
        this.patFormerDescription = patFormerDescription;
    }
    public String getAdmissionsDoctor() {
        return admissionsDoctor;
    }

    public void setAdmissionsDoctor(String admissionsDoctor) {
        this.admissionsDoctor = admissionsDoctor;
    }
    public String getAdmissionsDoctorId() {
        return admissionsDoctorId;
    }

    public void setAdmissionsDoctorId(String admissionsDoctorId) {
        this.admissionsDoctorId = admissionsDoctorId;
    }
    public String getReferringDoctor() {
        return referringDoctor;
    }

    public void setReferringDoctor(String referringDoctor) {
        this.referringDoctor = referringDoctor;
    }
    public String getReferringDoctorId() {
        return referringDoctorId;
    }

    public void setReferringDoctorId(String referringDoctorId) {
        this.referringDoctorId = referringDoctorId;
    }
    public String getConsultationDoctor() {
        return consultationDoctor;
    }

    public void setConsultationDoctor(String consultationDoctor) {
        this.consultationDoctor = consultationDoctor;
    }
    public String getConsultationDoctorId() {
        return consultationDoctorId;
    }

    public void setConsultationDoctorId(String consultationDoctorId) {
        this.consultationDoctorId = consultationDoctorId;
    }
    public String getHospitalService() {
        return hospitalService;
    }

    public void setHospitalService(String hospitalService) {
        this.hospitalService = hospitalService;
    }
    public String getPatTempPointOfCare() {
        return patTempPointOfCare;
    }

    public void setPatTempPointOfCare(String patTempPointOfCare) {
        this.patTempPointOfCare = patTempPointOfCare;
    }
    public String getPatTempRoom() {
        return patTempRoom;
    }

    public void setPatTempRoom(String patTempRoom) {
        this.patTempRoom = patTempRoom;
    }
    public String getPatTempBed() {
        return patTempBed;
    }

    public void setPatTempBed(String patTempBed) {
        this.patTempBed = patTempBed;
    }
    public String getPatTempDep() {
        return patTempDep;
    }

    public void setPatTempDep(String patTempDep) {
        this.patTempDep = patTempDep;
    }
    public String getPatTempPositionStatus() {
        return patTempPositionStatus;
    }

    public void setPatTempPositionStatus(String patTempPositionStatus) {
        this.patTempPositionStatus = patTempPositionStatus;
    }
    public String getPatTempPositionType() {
        return patTempPositionType;
    }

    public void setPatTempPositionType(String patTempPositionType) {
        this.patTempPositionType = patTempPositionType;
    }
    public String getPatTempBuilding() {
        return patTempBuilding;
    }

    public void setPatTempBuilding(String patTempBuilding) {
        this.patTempBuilding = patTempBuilding;
    }
    public String getPatTempFloor() {
        return patTempFloor;
    }

    public void setPatTempFloor(String patTempFloor) {
        this.patTempFloor = patTempFloor;
    }
    public String getPatTempDescription() {
        return patTempDescription;
    }

    public void setPatTempDescription(String patTempDescription) {
        this.patTempDescription = patTempDescription;
    }
    public String getPatAdmissionTest() {
        return patAdmissionTest;
    }

    public void setPatAdmissionTest(String patAdmissionTest) {
        this.patAdmissionTest = patAdmissionTest;
    }
    public String getPatReAdmission() {
        return patReAdmission;
    }

    public void setPatReAdmission(String patReAdmission) {
        this.patReAdmission = patReAdmission;
    }
    public String getPatAdmissionSource() {
        return patAdmissionSource;
    }

    public void setPatAdmissionSource(String patAdmissionSource) {
        this.patAdmissionSource = patAdmissionSource;
    }
    public String getPatAmbulatoryStatus() {
        return patAmbulatoryStatus;
    }

    public void setPatAmbulatoryStatus(String patAmbulatoryStatus) {
        this.patAmbulatoryStatus = patAmbulatoryStatus;
    }
    public String getPatVip() {
        return patVip;
    }

    public void setPatVip(String patVip) {
        this.patVip = patVip;
    }
    public String getPatAdmissionDoctors() {
        return patAdmissionDoctors;
    }

    public void setPatAdmissionDoctors(String patAdmissionDoctors) {
        this.patAdmissionDoctors = patAdmissionDoctors;
    }
    public String getPatAdmissionDoctorsId() {
        return patAdmissionDoctorsId;
    }

    public void setPatAdmissionDoctorsId(String patAdmissionDoctorsId) {
        this.patAdmissionDoctorsId = patAdmissionDoctorsId;
    }
    public String getPatientClass() {
        if(StringUtils.isEmpty(patientClass)){
            this.patientClass = DEFAULT_INHOS_TYPE;
        }
        return patientClass;
    }

    public void setPatientClass(String patientClass) {
        this.patientClass = patientClass;
    }
    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }
    public String getPatFinancialClass() {
        return patFinancialClass;
    }

    public void setPatFinancialClass(String patFinancialClass) {
        this.patFinancialClass = patFinancialClass;
    }
    public String getRoomBedCostPrice() {
        return roomBedCostPrice;
    }

    public void setRoomBedCostPrice(String roomBedCostPrice) {
        this.roomBedCostPrice = roomBedCostPrice;
    }
    public String getCourtesyCode() {
        return courtesyCode;
    }

    public void setCourtesyCode(String courtesyCode) {
        this.courtesyCode = courtesyCode;
    }
    public String getCreditRating() {
        return creditRating;
    }

    public void setCreditRating(String creditRating) {
        this.creditRating = creditRating;
    }
    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }
    public String getContractCreateDate() {
        return contractCreateDate;
    }

    public void setContractCreateDate(String contractCreateDate) {
        this.contractCreateDate = contractCreateDate;
    }
    public String getContractPrice() {
        return contractPrice;
    }

    public void setContractPrice(String contractPrice) {
        this.contractPrice = contractPrice;
    }
    public String getContractTime() {
        return contractTime;
    }

    public void setContractTime(String contractTime) {
        this.contractTime = contractTime;
    }
    public String getInterestRateCode() {
        return interestRateCode;
    }

    public void setInterestRateCode(String interestRateCode) {
        this.interestRateCode = interestRateCode;
    }
    public String getBadDebts() {
        return badDebts;
    }

    public void setBadDebts(String badDebts) {
        this.badDebts = badDebts;
    }
    public String getBadDebtsCreateDate() {
        return badDebtsCreateDate;
    }

    public void setBadDebtsCreateDate(String badDebtsCreateDate) {
        this.badDebtsCreateDate = badDebtsCreateDate;
    }
    public String getBadDebtsCode() {
        return badDebtsCode;
    }

    public void setBadDebtsCode(String badDebtsCode) {
        this.badDebtsCode = badDebtsCode;
    }
    public String getBadDebtsPrice() {
        return badDebtsPrice;
    }

    public void setBadDebtsPrice(String badDebtsPrice) {
        this.badDebtsPrice = badDebtsPrice;
    }
    //public String getBadDebtsRestorePrice() {
    //    return badDebtsRestorePrice;
    //}
    //
    //public void setBadDebtsRestorePrice(String badDebtsRestorePrice) {
    //    this.badDebtsRestorePrice = badDebtsRestorePrice;
    //}
    public String getPatAccountVoided() {
        return patAccountVoided;
    }

    public void setPatAccountVoided(String patAccountVoided) {
        this.patAccountVoided = patAccountVoided;
    }
    public String getPatAccountVoidedDate() {
        return patAccountVoidedDate;
    }

    public void setPatAccountVoidedDate(String patAccountVoidedDate) {
        this.patAccountVoidedDate = patAccountVoidedDate;
    }
    public String getPatDischargeDisposition() {
        return patDischargeDisposition;
    }

    public void setPatDischargeDisposition(String patDischargeDisposition) {
        this.patDischargeDisposition = patDischargeDisposition;
    }
    public String getPatDischargeLocation() {
        return patDischargeLocation;
    }

    public void setPatDischargeLocation(String patDischargeLocation) {
        this.patDischargeLocation = patDischargeLocation;
    }
    public String getPatDietType() {
        return patDietType;
    }

    public void setPatDietType(String patDietType) {
        this.patDietType = patDietType;
    }
    public String getPatServiceAgencies() {
        return patServiceAgencies;
    }

    public void setPatServiceAgencies(String patServiceAgencies) {
        this.patServiceAgencies = patServiceAgencies;
    }
    public String getPatBedStatus() {
        return patBedStatus;
    }

    public void setPatBedStatus(String patBedStatus) {
        this.patBedStatus = patBedStatus;
    }
    public String getPatAccountStatus() {
        return patAccountStatus;
    }

    public void setPatAccountStatus(String patAccountStatus) {
        this.patAccountStatus = patAccountStatus;
    }
    public String getPatDeterPointOfCare() {
        return patDeterPointOfCare;
    }

    public void setPatDeterPointOfCare(String patDeterPointOfCare) {
        this.patDeterPointOfCare = patDeterPointOfCare;
    }
    public String getPatDeterRoom() {
        return patDeterRoom;
    }

    public void setPatDeterRoom(String patDeterRoom) {
        this.patDeterRoom = patDeterRoom;
    }
    public String getPatDeterBed() {
        return patDeterBed;
    }

    public void setPatDeterBed(String patDeterBed) {
        this.patDeterBed = patDeterBed;
    }
    public String getPatDeterDep() {
        return patDeterDep;
    }

    public void setPatDeterDep(String patDeterDep) {
        this.patDeterDep = patDeterDep;
    }
    public String getPatDeterPositionStatus() {
        return patDeterPositionStatus;
    }

    public void setPatDeterPositionStatus(String patDeterPositionStatus) {
        this.patDeterPositionStatus = patDeterPositionStatus;
    }
    public String getPatDeterPositionType() {
        return patDeterPositionType;
    }

    public void setPatDeterPositionType(String patDeterPositionType) {
        this.patDeterPositionType = patDeterPositionType;
    }
    public String getPatDeterBuilding() {
        return patDeterBuilding;
    }

    public void setPatDeterBuilding(String patDeterBuilding) {
        this.patDeterBuilding = patDeterBuilding;
    }
    public String getPatDeterFloor() {
        return patDeterFloor;
    }

    public void setPatDeterFloor(String patDeterFloor) {
        this.patDeterFloor = patDeterFloor;
    }
    public String getPatDeterDescription() {
        return patDeterDescription;
    }

    public void setPatDeterDescription(String patDeterDescription) {
        this.patDeterDescription = patDeterDescription;
    }
    public String getPatForTempPointOfCare() {
        return patForTempPointOfCare;
    }

    public void setPatForTempPointOfCare(String patForTempPointOfCare) {
        this.patForTempPointOfCare = patForTempPointOfCare;
    }
    public String getPatForTempRoom() {
        return patForTempRoom;
    }

    public void setPatForTempRoom(String patForTempRoom) {
        this.patForTempRoom = patForTempRoom;
    }
    public String getPatForTempBed() {
        return patForTempBed;
    }

    public void setPatForTempBed(String patForTempBed) {
        this.patForTempBed = patForTempBed;
    }
    public String getPatForTempDep() {
        return patForTempDep;
    }

    public void setPatForTempDep(String patForTempDep) {
        this.patForTempDep = patForTempDep;
    }
    public String getPatForTempPositionStatus() {
        return patForTempPositionStatus;
    }

    public void setPatForTempPositionStatus(String patForTempPositionStatus) {
        this.patForTempPositionStatus = patForTempPositionStatus;
    }
    public String getPatForTempPositionType() {
        return patForTempPositionType;
    }

    public void setPatForTempPositionType(String patForTempPositionType) {
        this.patForTempPositionType = patForTempPositionType;
    }
    public String getPatForTempBuilding() {
        return patForTempBuilding;
    }

    public void setPatForTempBuilding(String patForTempBuilding) {
        this.patForTempBuilding = patForTempBuilding;
    }
    public String getPatForTempFloor() {
        return patForTempFloor;
    }

    public void setPatForTempFloor(String patForTempFloor) {
        this.patForTempFloor = patForTempFloor;
    }
    public String getPatForTempDescription() {
        return patForTempDescription;
    }

    public void setPatForTempDescription(String patForTempDescription) {
        this.patForTempDescription = patForTempDescription;
    }
    public Date getAdmitDate() {
        return admitDate;
    }

    public void setAdmitDate(Date admitDate) {
        this.admitDate = admitDate;
    }
    public Date getDischargeDate() {
        return dischargeDate;
    }

    public void setDischargeDate(Date dischargeDate) {
        this.dischargeDate = dischargeDate;
    }
    public String getPatDifference() {
        return patDifference;
    }

    public void setPatDifference(String patDifference) {
        this.patDifference = patDifference;
    }
    public String getPatTotalCost() {
        return patTotalCost;
    }

    public void setPatTotalCost(String patTotalCost) {
        this.patTotalCost = patTotalCost;
    }
    public String getPatTotalDispatch() {
        return patTotalDispatch;
    }

    public void setPatTotalDispatch(String patTotalDispatch) {
        this.patTotalDispatch = patTotalDispatch;
    }
    public String getPatTotalAmountPayable() {
        return patTotalAmountPayable;
    }

    public void setPatTotalAmountPayable(String patTotalAmountPayable) {
        this.patTotalAmountPayable = patTotalAmountPayable;
    }
    public String getPatSpareId() {
        return patSpareId;
    }

    public void setPatSpareId(String patSpareId) {
        this.patSpareId = patSpareId;
    }
    public String getPatVisitLogo() {
        return patVisitLogo;
    }

    public void setPatVisitLogo(String patVisitLogo) {
        this.patVisitLogo = patVisitLogo;
    }
    public String getOtherMedicalInstitutions() {
        return otherMedicalInstitutions;
    }

    public void setOtherMedicalInstitutions(String otherMedicalInstitutions) {
        this.otherMedicalInstitutions = otherMedicalInstitutions;
    }
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    public String getCreateId() {
        return createId;
    }

    public void setCreateId(String createId) {
        this.createId = createId;
    }
    public Date getVoidedDate() {
        return voidedDate;
    }

    public void setVoidedDate(Date voidedDate) {
        this.voidedDate = voidedDate;
    }
    public String getVoidedId() {
        return voidedId;
    }

    public void setVoidedId(String voidedId) {
        this.voidedId = voidedId;
    }
    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }
    public String getModifyId() {
        return modifyId;
    }

    public void setModifyId(String modifyId) {
        this.modifyId = modifyId;
    }
    public String getCustom1() {
        return custom1;
    }

    public void setCustom1(String custom1) {
        this.custom1 = custom1;
    }
    public String getCustom2() {
        return custom2;
    }

    public void setCustom2(String custom2) {
        this.custom2 = custom2;
    }
    public String getCustom3() {
        return custom3;
    }

    public void setCustom3(String custom3) {
        this.custom3 = custom3;
    }
    public String getCustom4() {
        return custom4;
    }

    public void setCustom4(String custom4) {
        this.custom4 = custom4;
    }
    public String getCustom5() {
        return custom5;
    }

    public void setCustom5(String custom5) {
        this.custom5 = custom5;
    }
    public String getPatNurseCode() {
        return patNurseCode;
    }

    public void setPatNurseCode(String patNurseCode) {
        this.patNurseCode = patNurseCode;
    }
    public String getPatNurseName() {
        return patNurseName;
    }

    public void setPatNurseName(String patNurseName) {
        this.patNurseName = patNurseName;
    }
    public String getTend() {
        return tend;
    }

    public void setTend(String tend) {
        this.tend = tend;
    }
    public String getBabyFlag() {
        return babyFlag;
    }

    public void setBabyFlag(String babyFlag) {
        this.babyFlag = babyFlag;
    }
    public String getAdmitWeight() {
        return admitWeight;
    }

    public void setAdmitWeight(String admitWeight) {
        this.admitWeight = admitWeight;
    }
    public String getBirthWeight() {
        return birthWeight;
    }

    public void setBirthWeight(String birthWeight) {
        this.birthWeight = birthWeight;
    }
    public String getOperCode() {
        return operCode;
    }

    public void setOperCode(String operCode) {
        this.operCode = operCode;
    }
    public Date getOperDate() {
        return operDate;
    }

    public void setOperDate(Date operDate) {
        this.operDate = operDate;
    }
    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    public String getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(String insuranceType) {
        this.insuranceType = insuranceType;
    }
    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }
    public String getContactRelations() {
        return contactRelations;
    }

    public void setContactRelations(String contactRelations) {
        this.contactRelations = contactRelations;
    }
    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }
    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }
    public String getPatCategoryName() {
        return patCategoryName;
    }

    public void setPatCategoryName(String patCategoryName) {
        this.patCategoryName = patCategoryName;
    }
    public String getGenderName() {
        return genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }
    public String getPayRate() {
        return payRate;
    }

    public void setPayRate(String payRate) {
        this.payRate = payRate;
    }
    public String getDischargeName() {
        return dischargeName;
    }

    public void setDischargeName(String dischargeName) {
        this.dischargeName = dischargeName;
    }
    public String getInsuranceName() {
        return insuranceName;
    }

    public void setInsuranceName(String insuranceName) {
        this.insuranceName = insuranceName;
    }
    public String getAdmissionName() {
        return admissionName;
    }

    public void setAdmissionName(String admissionName) {
        this.admissionName = admissionName;
    }
    public String getIpStatusName() {
        return ipStatusName;
    }

    public void setIpStatusName(String ipStatusName) {
        this.ipStatusName = ipStatusName;
    }
    public String getDificultyName() {
        return dificultyName;
    }

    public void setDificultyName(String dificultyName) {
        this.dificultyName = dificultyName;
    }
    public String getAdmitWayName() {
        return admitWayName;
    }

    public void setAdmitWayName(String admitWayName) {
        this.admitWayName = admitWayName;
    }
    public String getAdmitWeightUnit() {
        return admitWeightUnit;
    }

    public void setAdmitWeightUnit(String admitWeightUnit) {
        this.admitWeightUnit = admitWeightUnit;
    }
    public String getBabyWeightUnit() {
        return babyWeightUnit;
    }

    public void setBabyWeightUnit(String babyWeightUnit) {
        this.babyWeightUnit = babyWeightUnit;
    }
    public String getMedicinelimitamount() {
        return medicinelimitamount;
    }

    public void setMedicinelimitamount(String medicinelimitamount) {
        this.medicinelimitamount = medicinelimitamount;
    }
    public String getSickbedlimitamount() {
        return sickbedlimitamount;
    }

    public void setSickbedlimitamount(String sickbedlimitamount) {
        this.sickbedlimitamount = sickbedlimitamount;
    }
    public String getExaminelimitamount() {
        return examinelimitamount;
    }

    public void setExaminelimitamount(String examinelimitamount) {
        this.examinelimitamount = examinelimitamount;
    }
    public String getCurelimitamount() {
        return curelimitamount;
    }

    public void setCurelimitamount(String curelimitamount) {
        this.curelimitamount = curelimitamount;
    }
    public String getHiupStatus() {
        return hiupStatus;
    }

    public void setHiupStatus(String hiupStatus) {
        this.hiupStatus = hiupStatus;
    }
    public String getHiupErrorInfo() {
        return hiupErrorInfo;
    }

    public void setHiupErrorInfo(String hiupErrorInfo) {
        this.hiupErrorInfo = hiupErrorInfo;
    }
    public String getAdmissionDomain() {
        return admissionDomain;
    }

    public void setAdmissionDomain(String admissionDomain) {
        this.admissionDomain = admissionDomain;
    }
    public String getAdmissionSourceDomain() {
        return admissionSourceDomain;
    }

    public void setAdmissionSourceDomain(String admissionSourceDomain) {
        this.admissionSourceDomain = admissionSourceDomain;
    }
    public String getAdmissionSourceName() {
        return admissionSourceName;
    }

    public void setAdmissionSourceName(String admissionSourceName) {
        this.admissionSourceName = admissionSourceName;
    }
    public String getPatientClassName() {
        return patientClassName;
    }

    public void setPatientClassName(String patientClassName) {
        this.patientClassName = patientClassName;
    }
    public String getPatientClassDomain() {
        return patientClassDomain;
    }

    public void setPatientClassDomain(String patientClassDomain) {
        this.patientClassDomain = patientClassDomain;
    }
    public String getIpStatusDomain() {
        return ipStatusDomain;
    }

    public void setIpStatusDomain(String ipStatusDomain) {
        this.ipStatusDomain = ipStatusDomain;
    }
    public String getDificultyDomain() {
        return dificultyDomain;
    }

    public void setDificultyDomain(String dificultyDomain) {
        this.dificultyDomain = dificultyDomain;
    }
    public String getDischargeDomain() {
        return dischargeDomain;
    }

    public void setDischargeDomain(String dischargeDomain) {
        this.dischargeDomain = dischargeDomain;
    }
    public String getAccountStatusName() {
        return accountStatusName;
    }

    public void setAccountStatusName(String accountStatusName) {
        this.accountStatusName = accountStatusName;
    }
    public String getAccountStatusDomain() {
        return accountStatusDomain;
    }

    public void setAccountStatusDomain(String accountStatusDomain) {
        this.accountStatusDomain = accountStatusDomain;
    }
    public String getGenderDomain() {
        return genderDomain;
    }

    public void setGenderDomain(String genderDomain) {
        this.genderDomain = genderDomain;
    }
    public String getPatCategorySystem() {
        return patCategorySystem;
    }

    public void setPatCategorySystem(String patCategorySystem) {
        this.patCategorySystem = patCategorySystem;
    }
    public String getMothersId() {
        return mothersId;
    }

    public void setMothersId(String mothersId) {
        this.mothersId = mothersId;
    }
    public String getMothersDomain() {
        return mothersDomain;
    }

    public void setMothersDomain(String mothersDomain) {
        this.mothersDomain = mothersDomain;
    }
    public String getMothersFlowId() {
        return mothersFlowId;
    }

    public void setMothersFlowId(String mothersFlowId) {
        this.mothersFlowId = mothersFlowId;
    }
    public String getMothersFlowDomain() {
        return mothersFlowDomain;
    }

    public void setMothersFlowDomain(String mothersFlowDomain) {
        this.mothersFlowDomain = mothersFlowDomain;
    }
    public String getMothersName() {
        return mothersName;
    }

    public void setMothersName(String mothersName) {
        this.mothersName = mothersName;
    }
    public String getNoonCode() {
        return noonCode;
    }

    public void setNoonCode(String noonCode) {
        this.noonCode = noonCode;
    }
    public String getPaykindCode() {
        return paykindCode;
    }

    public void setPaykindCode(String paykindCode) {
        this.paykindCode = paykindCode;
    }
    public String getPaykindName() {
        return paykindName;
    }

    public void setPaykindName(String paykindName) {
        this.paykindName = paykindName;
    }
    public String getSchemaNo() {
        return schemaNo;
    }

    public void setSchemaNo(String schemaNo) {
        this.schemaNo = schemaNo;
    }
    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
    public String getSeeno() {
        return seeno;
    }

    public void setSeeno(String seeno) {
        this.seeno = seeno;
    }
    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    public String getYnbook() {
        return ynbook;
    }

    public void setYnbook(String ynbook) {
        this.ynbook = ynbook;
    }
    public String getYnfr() {
        return ynfr;
    }

    public void setYnfr(String ynfr) {
        this.ynfr = ynfr;
    }
    public String getAppendFlag() {
        return appendFlag;
    }

    public void setAppendFlag(String appendFlag) {
        this.appendFlag = appendFlag;
    }
    public String getYnsee() {
        return ynsee;
    }

    public void setYnsee(String ynsee) {
        this.ynsee = ynsee;
    }
    public Date getSeeDate() {
        return seeDate;
    }

    public void setSeeDate(Date seeDate) {
        this.seeDate = seeDate;
    }
    public String getTriageFlag() {
        return triageFlag;
    }

    public void setTriageFlag(String triageFlag) {
        this.triageFlag = triageFlag;
    }
    public String getTriageOpcd() {
        return triageOpcd;
    }

    public void setTriageOpcd(String triageOpcd) {
        this.triageOpcd = triageOpcd;
    }
    public Date getTriageDate() {
        return triageDate;
    }

    public void setTriageDate(Date triageDate) {
        this.triageDate = triageDate;
    }
    public String getSeeDpcd() {
        return seeDpcd;
    }

    public void setSeeDpcd(String seeDpcd) {
        this.seeDpcd = seeDpcd;
    }
    public String getSeeDocd() {
        return seeDocd;
    }

    public void setSeeDocd(String seeDocd) {
        this.seeDocd = seeDocd;
    }
    public String getOutPatientStatusA() {
        return outPatientStatusA;
    }

    public void setOutPatientStatusA(String outPatientStatusA) {
        this.outPatientStatusA = outPatientStatusA;
    }
    public String getOutPatientStatusB() {
        return outPatientStatusB;
    }

    public void setOutPatientStatusB(String outPatientStatusB) {
        this.outPatientStatusB = outPatientStatusB;
    }
    public String getOutPatientStatusC() {
        return outPatientStatusC;
    }

    public void setOutPatientStatusC(String outPatientStatusC) {
        this.outPatientStatusC = outPatientStatusC;
    }
    public String getInPatientStatusA() {
        return inPatientStatusA;
    }

    public void setInPatientStatusA(String inPatientStatusA) {
        this.inPatientStatusA = inPatientStatusA;
    }
    public String getInPatientStatusB() {
        return inPatientStatusB;
    }

    public void setInPatientStatusB(String inPatientStatusB) {
        this.inPatientStatusB = inPatientStatusB;
    }
    public String getInPatientStatusC() {
        return inPatientStatusC;
    }

    public void setInPatientStatusC(String inPatientStatusC) {
        this.inPatientStatusC = inPatientStatusC;
    }
    public String getPatientSourceName() {
        return patientSourceName;
    }

    public void setPatientSourceName(String patientSourceName) {
        this.patientSourceName = patientSourceName;
    }
    public String getOldPatientId() {
        return oldPatientId;
    }

    public void setOldPatientId(String oldPatientId) {
        this.oldPatientId = oldPatientId;
    }
    public String getOldPatientDomain() {
        return oldPatientDomain;
    }

    public void setOldPatientDomain(String oldPatientDomain) {
        this.oldPatientDomain = oldPatientDomain;
    }
    public String getOldVisitFlowId() {
        return oldVisitFlowId;
    }

    public void setOldVisitFlowId(String oldVisitFlowId) {
        this.oldVisitFlowId = oldVisitFlowId;
    }
    public String getOldVisitFlowDomain() {
        return oldVisitFlowDomain;
    }

    public void setOldVisitFlowDomain(String oldVisitFlowDomain) {
        this.oldVisitFlowDomain = oldVisitFlowDomain;
    }
    public String getOldVisitId() {
        return oldVisitId;
    }

    public void setOldVisitId(String oldVisitId) {
        this.oldVisitId = oldVisitId;
    }
    public String getOldPersonId() {
        return oldPersonId;
    }

    public void setOldPersonId(String oldPersonId) {
        this.oldPersonId = oldPersonId;
    }
    public String getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(String oldStatus) {
        this.oldStatus = oldStatus;
    }
    public String getOldInfo() {
        return oldInfo;
    }

    public void setOldInfo(String oldInfo) {
        this.oldInfo = oldInfo;
    }
    public String getIsemergency() {
        return isemergency;
    }

    public void setIsemergency(String isemergency) {
        this.isemergency = isemergency;
    }
    public String getPatIpTimes() {
        return patIpTimes;
    }

    public void setPatIpTimes(String patIpTimes) {
        this.patIpTimes = patIpTimes;
    }
    public String getDiagnoseIcd() {
        return diagnoseIcd;
    }

    public void setDiagnoseIcd(String diagnoseIcd) {
        this.diagnoseIcd = diagnoseIcd;
    }
    public String getDiagnoseName() {
        return diagnoseName;
    }

    public void setDiagnoseName(String diagnoseName) {
        this.diagnoseName = diagnoseName;
    }
    public String getCustom6() {
        return custom6;
    }

    public void setCustom6(String custom6) {
        this.custom6 = custom6;
    }
    public String getRelevanceId() {
        return relevanceId;
    }

    public void setRelevanceId(String relevanceId) {
        this.relevanceId = relevanceId;
    }
    public String getRelevanceDomain() {
        return relevanceDomain;
    }

    public void setRelevanceDomain(String relevanceDomain) {
        this.relevanceDomain = relevanceDomain;
    }
    public String getRelevanceName() {
        return relevanceName;
    }

    public void setRelevanceName(String relevanceName) {
        this.relevanceName = relevanceName;
    }
    public String getDiagnosisTreatmentGroup() {
        return diagnosisTreatmentGroup;
    }

    public void setDiagnosisTreatmentGroup(String diagnosisTreatmentGroup) {
        this.diagnosisTreatmentGroup = diagnosisTreatmentGroup;
    }
    public String getDiagnosisTreatmentGroupcode() {
        return diagnosisTreatmentGroupcode;
    }

    public void setDiagnosisTreatmentGroupcode(String diagnosisTreatmentGroupcode) {
        this.diagnosisTreatmentGroupcode = diagnosisTreatmentGroupcode;
    }
    public String getNursingGroup() {
        return nursingGroup;
    }

    public void setNursingGroup(String nursingGroup) {
        this.nursingGroup = nursingGroup;
    }
    public String getNursingGroupCode() {
        return nursingGroupCode;
    }

    public void setNursingGroupCode(String nursingGroupCode) {
        this.nursingGroupCode = nursingGroupCode;
    }
    public String getHospitalizationPurposes() {
        return hospitalizationPurposes;
    }

    public void setHospitalizationPurposes(String hospitalizationPurposes) {
        this.hospitalizationPurposes = hospitalizationPurposes;
    }
    public String getTubeBedDoctor() {
        return tubeBedDoctor;
    }

    public void setTubeBedDoctor(String tubeBedDoctor) {
        this.tubeBedDoctor = tubeBedDoctor;
    }
    public String getOperName() {
        return operName;
    }

    public void setOperName(String operName) {
        this.operName = operName;
    }
    public String getEnterDeptTime() {
        return enterDeptTime;
    }

    public void setEnterDeptTime(String enterDeptTime) {
        this.enterDeptTime = enterDeptTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.visitId;
    }

    @Override
    public String toString() {
        return "PatientVisit{" +
        "visitId=" + visitId +
        ", personId=" + personId +
        ", personDomain=" + personDomain +
        ", visitFlowId=" + visitFlowId +
        ", visitFlowDomain=" + visitFlowDomain +
        ", hospitalDomain=" + hospitalDomain +
        ", patCategory=" + patCategory +
        ", patCurrentPointOfCare=" + patCurrentPointOfCare +
        ", patCurrentRoom=" + patCurrentRoom +
        ", patCurrentBed=" + patCurrentBed +
        ", patCuurentDep=" + patCuurentDep +
        ", patCurrentPositionStatus=" + patCurrentPositionStatus +
        ", patCurrentPositionType=" + patCurrentPositionType +
        ", patCurrentBuilding=" + patCurrentBuilding +
        ", patCurrentFloor=" + patCurrentFloor +
        ", patCuurentDescription=" + patCuurentDescription +
        ", patAdmissionType=" + patAdmissionType +
        ", patAdmissionNumber=" + patAdmissionNumber +
        ", patFormerPointOfCare=" + patFormerPointOfCare +
        ", patFormerRoom=" + patFormerRoom +
        ", patFormerBed=" + patFormerBed +
        ", patFormerDep=" + patFormerDep +
        ", patFormerPositionStatus=" + patFormerPositionStatus +
        ", patFormerPositionType=" + patFormerPositionType +
        ", patFormerBuilding=" + patFormerBuilding +
        ", patFormerFloor=" + patFormerFloor +
        ", patFormerDescription=" + patFormerDescription +
        ", admissionsDoctor=" + admissionsDoctor +
        ", admissionsDoctorId=" + admissionsDoctorId +
        ", referringDoctor=" + referringDoctor +
        ", referringDoctorId=" + referringDoctorId +
        ", consultationDoctor=" + consultationDoctor +
        ", consultationDoctorId=" + consultationDoctorId +
        ", hospitalService=" + hospitalService +
        ", patTempPointOfCare=" + patTempPointOfCare +
        ", patTempRoom=" + patTempRoom +
        ", patTempBed=" + patTempBed +
        ", patTempDep=" + patTempDep +
        ", patTempPositionStatus=" + patTempPositionStatus +
        ", patTempPositionType=" + patTempPositionType +
        ", patTempBuilding=" + patTempBuilding +
        ", patTempFloor=" + patTempFloor +
        ", patTempDescription=" + patTempDescription +
        ", patAdmissionTest=" + patAdmissionTest +
        ", patReAdmission=" + patReAdmission +
        ", patAdmissionSource=" + patAdmissionSource +
        ", patAmbulatoryStatus=" + patAmbulatoryStatus +
        ", patVip=" + patVip +
        ", patAdmissionDoctors=" + patAdmissionDoctors +
        ", patAdmissionDoctorsId=" + patAdmissionDoctorsId +
        ", patientClass=" + patientClass +
        ", patientId=" + patientId +
        ", patFinancialClass=" + patFinancialClass +
        ", roomBedCostPrice=" + roomBedCostPrice +
        ", courtesyCode=" + courtesyCode +
        ", creditRating=" + creditRating +
        ", contractCode=" + contractCode +
        ", contractCreateDate=" + contractCreateDate +
        ", contractPrice=" + contractPrice +
        ", contractTime=" + contractTime +
        ", interestRateCode=" + interestRateCode +
        ", badDebts=" + badDebts +
        ", badDebtsCreateDate=" + badDebtsCreateDate +
        ", badDebtsCode=" + badDebtsCode +
        ", badDebtsPrice=" + badDebtsPrice +
        //", badDebtsRestorePrice=" + badDebtsRestorePrice +
        ", patAccountVoided=" + patAccountVoided +
        ", patAccountVoidedDate=" + patAccountVoidedDate +
        ", patDischargeDisposition=" + patDischargeDisposition +
        ", patDischargeLocation=" + patDischargeLocation +
        ", patDietType=" + patDietType +
        ", patServiceAgencies=" + patServiceAgencies +
        ", patBedStatus=" + patBedStatus +
        ", patAccountStatus=" + patAccountStatus +
        ", patDeterPointOfCare=" + patDeterPointOfCare +
        ", patDeterRoom=" + patDeterRoom +
        ", patDeterBed=" + patDeterBed +
        ", patDeterDep=" + patDeterDep +
        ", patDeterPositionStatus=" + patDeterPositionStatus +
        ", patDeterPositionType=" + patDeterPositionType +
        ", patDeterBuilding=" + patDeterBuilding +
        ", patDeterFloor=" + patDeterFloor +
        ", patDeterDescription=" + patDeterDescription +
        ", patForTempPointOfCare=" + patForTempPointOfCare +
        ", patForTempRoom=" + patForTempRoom +
        ", patForTempBed=" + patForTempBed +
        ", patForTempDep=" + patForTempDep +
        ", patForTempPositionStatus=" + patForTempPositionStatus +
        ", patForTempPositionType=" + patForTempPositionType +
        ", patForTempBuilding=" + patForTempBuilding +
        ", patForTempFloor=" + patForTempFloor +
        ", patForTempDescription=" + patForTempDescription +
        ", admitDate=" + admitDate +
        ", dischargeDate=" + dischargeDate +
        ", patDifference=" + patDifference +
        ", patTotalCost=" + patTotalCost +
        ", patTotalDispatch=" + patTotalDispatch +
        ", patTotalAmountPayable=" + patTotalAmountPayable +
        ", patSpareId=" + patSpareId +
        ", patVisitLogo=" + patVisitLogo +
        ", otherMedicalInstitutions=" + otherMedicalInstitutions +
        ", createDate=" + createDate +
        ", createId=" + createId +
        ", voidedDate=" + voidedDate +
        ", voidedId=" + voidedId +
        ", modifyDate=" + modifyDate +
        ", modifyId=" + modifyId +
        ", custom1=" + custom1 +
        ", custom2=" + custom2 +
        ", custom3=" + custom3 +
        ", custom4=" + custom4 +
        ", custom5=" + custom5 +
        ", patNurseCode=" + patNurseCode +
        ", patNurseName=" + patNurseName +
        ", tend=" + tend +
        ", babyFlag=" + babyFlag +
        ", admitWeight=" + admitWeight +
        ", birthWeight=" + birthWeight +
        ", operCode=" + operCode +
        ", operDate=" + operDate +
        ", prefix=" + prefix +
        ", insuranceType=" + insuranceType +
        ", contactPerson=" + contactPerson +
        ", contactRelations=" + contactRelations +
        ", contactAddress=" + contactAddress +
        ", contactPhone=" + contactPhone +
        ", patCategoryName=" + patCategoryName +
        ", genderName=" + genderName +
        ", payRate=" + payRate +
        ", dischargeName=" + dischargeName +
        ", insuranceName=" + insuranceName +
        ", admissionName=" + admissionName +
        ", ipStatusName=" + ipStatusName +
        ", dificultyName=" + dificultyName +
        ", admitWayName=" + admitWayName +
        ", admitWeightUnit=" + admitWeightUnit +
        ", babyWeightUnit=" + babyWeightUnit +
        ", medicinelimitamount=" + medicinelimitamount +
        ", sickbedlimitamount=" + sickbedlimitamount +
        ", examinelimitamount=" + examinelimitamount +
        ", curelimitamount=" + curelimitamount +
        ", hiupStatus=" + hiupStatus +
        ", hiupErrorInfo=" + hiupErrorInfo +
        ", admissionDomain=" + admissionDomain +
        ", admissionSourceDomain=" + admissionSourceDomain +
        ", admissionSourceName=" + admissionSourceName +
        ", patientClassName=" + patientClassName +
        ", patientClassDomain=" + patientClassDomain +
        ", ipStatusDomain=" + ipStatusDomain +
        ", dificultyDomain=" + dificultyDomain +
        ", dischargeDomain=" + dischargeDomain +
        ", accountStatusName=" + accountStatusName +
        ", accountStatusDomain=" + accountStatusDomain +
        ", genderDomain=" + genderDomain +
        ", patCategorySystem=" + patCategorySystem +
        ", mothersId=" + mothersId +
        ", mothersDomain=" + mothersDomain +
        ", mothersFlowId=" + mothersFlowId +
        ", mothersFlowDomain=" + mothersFlowDomain +
        ", mothersName=" + mothersName +
        ", noonCode=" + noonCode +
        ", paykindCode=" + paykindCode +
        ", paykindName=" + paykindName +
        ", schemaNo=" + schemaNo +
        ", orderNo=" + orderNo +
        ", seeno=" + seeno +
        ", beginTime=" + beginTime +
        ", endTime=" + endTime +
        ", ynbook=" + ynbook +
        ", ynfr=" + ynfr +
        ", appendFlag=" + appendFlag +
        ", ynsee=" + ynsee +
        ", seeDate=" + seeDate +
        ", triageFlag=" + triageFlag +
        ", triageOpcd=" + triageOpcd +
        ", triageDate=" + triageDate +
        ", seeDpcd=" + seeDpcd +
        ", seeDocd=" + seeDocd +
        ", outPatientStatusA=" + outPatientStatusA +
        ", outPatientStatusB=" + outPatientStatusB +
        ", outPatientStatusC=" + outPatientStatusC +
        ", inPatientStatusA=" + inPatientStatusA +
        ", inPatientStatusB=" + inPatientStatusB +
        ", inPatientStatusC=" + inPatientStatusC +
        ", patientSourceName=" + patientSourceName +
        ", oldPatientId=" + oldPatientId +
        ", oldPatientDomain=" + oldPatientDomain +
        ", oldVisitFlowId=" + oldVisitFlowId +
        ", oldVisitFlowDomain=" + oldVisitFlowDomain +
        ", oldVisitId=" + oldVisitId +
        ", oldPersonId=" + oldPersonId +
        ", oldStatus=" + oldStatus +
        ", oldInfo=" + oldInfo +
        ", isemergency=" + isemergency +
        ", patIpTimes=" + patIpTimes +
        ", diagnoseIcd=" + diagnoseIcd +
        ", diagnoseName=" + diagnoseName +
        ", custom6=" + custom6 +
        ", relevanceId=" + relevanceId +
        ", relevanceDomain=" + relevanceDomain +
        ", relevanceName=" + relevanceName +
        ", diagnosisTreatmentGroup=" + diagnosisTreatmentGroup +
        ", diagnosisTreatmentGroupcode=" + diagnosisTreatmentGroupcode +
        ", nursingGroup=" + nursingGroup +
        ", nursingGroupCode=" + nursingGroupCode +
        ", hospitalizationPurposes=" + hospitalizationPurposes +
        ", tubeBedDoctor=" + tubeBedDoctor +
        ", operName=" + operName +
        ", enterDeptTime=" + enterDeptTime +
        "}";
    }
}
