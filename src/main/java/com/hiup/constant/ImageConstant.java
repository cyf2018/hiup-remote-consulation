package com.hiup.constant;

/**
 * @author cyf
 * @description     影像接口常量
 * @create 2018-12-26 18:41
 **/
public class ImageConstant {

    /**
     * 调用影像接口传入的参数名称 empi & patientDomainId
     */
    public static final String IMAGE_EMPI = "empi";
    public static final String IMAGE_PATIENT_DOMAIN_ID = "patientDomainId";
}
