package com.hiup.constant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cyf
 * @description     荷载类型映射配置
 * @create 2018-12-26 13:44
 **/
public class PayLoadTypeConstant {
    private static final Logger log = LoggerFactory.getLogger(PayLoadTypeConstant.class);

    private static Map<String,Integer> PAY_LOAD_TYPE_MAP = new HashMap<>();

    {
        PAY_LOAD_TYPE_MAP.put("XDS.FrontPage",RECORD_HOME_PAGE);
        PAY_LOAD_TYPE_MAP.put("XDS.ABW.InchargeRecord",HOSPITALIZATION_RECORD);
        PAY_LOAD_TYPE_MAP.put("XDS.ProgressNote.0001",DISEASE_RECORD);
        PAY_LOAD_TYPE_MAP.put("XDS.ProgressNote.008",DISEASE_RECORD);
        PAY_LOAD_TYPE_MAP.put("XDS.ProgressNote.0009",DISEASE_RECORD);
        PAY_LOAD_TYPE_MAP.put("XDS.ProgressNote.0020",DISEASE_RECORD);
        PAY_LOAD_TYPE_MAP.put("XDS.ABW.ProgressNote",DISEASE_RECORD);
        PAY_LOAD_TYPE_MAP.put("XDS.OperationInfoRec",OPERATION_RECORD);
        PAY_LOAD_TYPE_MAP.put("XDS.ProgressNote.2001",OPERATION_RECORD);
        PAY_LOAD_TYPE_MAP.put("XDS.UR2.NUCLEARBG",MEDICAL_LABORATORY);
        PAY_LOAD_TYPE_MAP.put("spring.config.medicalRrecord",PATHOLOGICAL_EXAMINATION);
        PAY_LOAD_TYPE_MAP.put("XDS.LJPISBG",PATHOLOGICAL_EXAMINATION);
        PAY_LOAD_TYPE_MAP.put("XDS.XM.PISBG",PATHOLOGICAL_EXAMINATION);
        PAY_LOAD_TYPE_MAP.put("XDS.PISRequest",PATHOLOGICAL_EXAMINATION);
        PAY_LOAD_TYPE_MAP.put("XDS.PISBG",PATHOLOGICAL_EXAMINATION);
        PAY_LOAD_TYPE_MAP.put("XDS.UR2.EISBG",ENDOSCOPE_EXAMINATION);
        PAY_LOAD_TYPE_MAP.put("XDS.EISBG",ENDOSCOPE_EXAMINATION);
        PAY_LOAD_TYPE_MAP.put("XDS.EBRBG",ENDOSCOPE_EXAMINATION);
        PAY_LOAD_TYPE_MAP.put("XDS.GEECGBG",EGG_EXAMINATION);
    }

    public static Map<String,Integer> map(){
        return PAY_LOAD_TYPE_MAP;
    }

    public static int getKey(String key){
        if(containsKey(key)){
            return PAY_LOAD_TYPE_MAP.get(key);
        }
        return DEFAULT_RECORD;
    }

    private static boolean containsKey(String key) {
        return PAY_LOAD_TYPE_MAP.containsKey(key);
    }


    /**
     * 病案首页
     */
    private static final int RECORD_HOME_PAGE = 1;

    /**
     * 入院记录
     */
    private static final int HOSPITALIZATION_RECORD = 2;

    /**
     * 病程记录
     */
    private static final int DISEASE_RECORD = 3;

    /**
     * 手术记录
     */
    private static final int OPERATION_RECORD = 4;

    /**
     * 医学检验
     */
    private static final int MEDICAL_LABORATORY = 5;

    /**
     * 影像检查(暂时此类型，默认为影像检查）
     */
    public static final int DEFAULT_RECORD = 6;

    /**
     * 病理检查
     */
    private static final int PATHOLOGICAL_EXAMINATION = 7;

    /**
     * 内镜检查
     */
    private static final int ENDOSCOPE_EXAMINATION = 8;

    /**
     * 心电检查
     */
    private static final int EGG_EXAMINATION = 9;

}
