package com.hiup.constant;

/**
 * @author cyf
 * @description 多数据源枚举
 * @create 2018-11-21 15:07
 **/
public enum DBTypeEnum {

    db1("db1"), db2("db2");

    private String value;

    DBTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
