package com.hiup.constant;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author cyf
 * @description
 * @create 2018-07-24 11:52
 **/
@Setter
@Getter
public class ResponseBean implements Serializable {
    private static final long serialVersionUID = 3913475391080065475L;

    /**
     * 是否接收成功 true 成功,false 失败
     */
    private boolean IsSuccess;

    /**
     * 接收失败原因   错误信息，成功无内容"
     */
    private String faultContext;

    public ResponseBean() {
        this.IsSuccess = true;
        this.faultContext="";
    }

    public ResponseBean(String faultContext, boolean isSuccess) {
        this.IsSuccess = isSuccess;
        this.faultContext=faultContext;
    }

    public static ResponseBean ok(){
        return new ResponseBean();
    }

    public static ResponseBean error(String faultContext){
        return new ResponseBean(faultContext,false);
    }

    public static ResponseBean error(){
        return new ResponseBean("操作失败",false);
    }

    @Override
    public String toString() {
        return "ResponseBean{" +
                "IsSuccess=" + IsSuccess +
                ", faultContext='" + faultContext + '\'' +
                '}';
    }
}
