package com.hiup.constant;

import com.hiup.utils.JSONUtil;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;

/**
 * @author cyf
 * @description 主数据常量
 * @create 2018-12-26 15:50
 **/
public class PathologyDataConstant {

    /**
     * 荷载类型
     */
    public static final String PAY_LOAD_TYPE = "pay_load_type";

    /**
     * 文件内容
     */
    public static final String FILE_META_LIST = "file_meta_list";

    /**
     * 结果数据
     */
    public static final String RESULT_DATA = "values";

    /**
     * 主诉
     */
    public static final String DJ_CAS_DE04 = "DJ_CAS_DE04_01_119_00";


    /**
     * 既往史
     */
    public static final String PAST_HISTORY = "ELAPSEMHIS";

    /**
     * 现病史
     */
    public static final String HISTORY_OF_PRESENT_ILLNESS = "DJ_HH_DE02_10_071_00";


    /**
     * 临床诊断 （取临床诊断先后顺序 最后诊断 - 门诊诊断 - 住院诊断）
     */
    public static final String LAST_DIAGNOSIS = "DJ_FINAL_DIAGNOSIS";
    public static final String OUTPATIENT_DEPARTMENT_DIAGNOSIS = "clinicaldiagnose";
    public static final String Hospitalization_DIAGNOSIS = "discharge_diagnosis";


    public static final String SEARCH_RESPONSES = "searchResponses";
    public static final String RESULT = "result";

    /**
     * 返回结果代码 0-成功 其他-失败
     */
    public static final String CODE = "code";

    /**
     *  文件id
     */
    public static final  String FILE_UID = "file_uid";

    /**
     *  文件名称
     */
    public static final String DOC_NAME = "doc_name";


    /**
     * 就诊号
     */
    public static final String INHOS_NO = "inhosNo";

    /**
     * 流水号
     */
    public static final String HIS_FLOW_ID = "hisFlowId";

    /**
     * 病历文档创建时间
     */
    public static final String DATE_CREATED = "date_created";
    public static final String PERSON_INFO_FIELD = "person_info_field";


    /**
     * 获取临床诊断
     *
     * @param pathologyData
     * @return
     */
    public static String getDiagnosis(JSONObject pathologyData) {
        String clinicalDiagnosis = "";
        if (StringUtils.isEmpty(JSONUtil.getValue(LAST_DIAGNOSIS, pathologyData))) {
            if (StringUtils.isEmpty(JSONUtil.getValue(OUTPATIENT_DEPARTMENT_DIAGNOSIS, pathologyData))) {
                clinicalDiagnosis = JSONUtil.getValue(Hospitalization_DIAGNOSIS, pathologyData);
            } else {
                clinicalDiagnosis = JSONUtil.getValue(OUTPATIENT_DEPARTMENT_DIAGNOSIS, pathologyData);
            }
        } else {
            clinicalDiagnosis = JSONUtil.getValue(LAST_DIAGNOSIS, pathologyData);
        }
        return clinicalDiagnosis;
    }

}
