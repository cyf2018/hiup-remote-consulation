package com.hiup.constant;

/**
 * @author cyf
 * @description 推送结果常量
 * @create 2018-12-26 18:39
 **/
public class PushResultConstant {

    /**
     * 推送信息是否成功(患者信息或病历信息)
     */
    public static final String IS_SUCCESS = "Issuccess";


    /**
     * 返回结果错误消息(患者信息或病历信息)
     */
    public static final String FAULT_CONTEXT = "Faultcontext";

}
