package com.hiup.constant;

import org.apache.commons.lang.StringUtils;

/**
 * 公共常量类
 *
 * @author YFC
 * @create 2018-12-20 上午 8:38
 */
public class GlobalConstant {

    /**
     * 创建时间，用于比较多个person对象,获取创建时间最大的对象.
     */
    public static final String DATE_CREATED = "dateCreated";

    /**
     * 入院日期,用于比较多个patientVisit对象,获取入院日期最近的
     * 的对象.
     */
    public static final String ADMIT_DATE = "admitDate";

    /**
     * id 和 域id截取的字符
     */
    public static final String SPLIT_DOMAIN = "\\^";

    /**
     *  状态，用于区分是否为影像接口的请求.
     *  影像接口get请求需要特殊处理请求参数.
     */
    public static final boolean IS_IMAGE_URL = true;

    /**
     *  主数据返回结果状态 0:正常, 其他：失败
     */
    public static final int PATHOLOGY_RESULT_CODE = 0;

    /**
     * 主数据文件扩展名
     */
    public static final String FILE_EXTENSION = "pdf";


    /**
     *  获取性别
     * @param sex   0:未知性别,1:男,2:女,9:未说明的性别 （1.7文档修改性别代码）
     * @return
     */
    public static Integer getSex(String sex) {
        if(StringUtils.isEmpty(sex)){
            return 9;
        }
        switch (sex) {
            case SEX.UNKNOWN:
                return 0;
            case SEX.MAN:
                return 1;
            case SEX.FEMALE:
                return 2;
            default:
                return 9;
        }
    }

    public static class SEX {
        /**
         * person 表中性别格式为：1-女，2-男 4- 未知
         */
        public static final String MAN = "2";
        public static final String FEMALE = "1";
        public static final String UNKNOWN = "4";
    }



}
