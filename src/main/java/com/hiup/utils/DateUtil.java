package com.hiup.utils;


import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * @author cyf
 * @description
 * @create 2018-07-25 16:55
 **/
public class DateUtil implements Converter<String, Date> {

    /**
     * 时间格式
     */
    private static String dateTimeFormats = "yyyyMMddHHmmss";
    private static SimpleDateFormat sdf = null;


    /**
     * 字符串转日期
     *
     * @param source
     * @return
     */
    public static Date str2Date(String source) {
        try {
            return getSdf().parse(source);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 日期转字符串
     *
     * @param date
     * @return
     */
    public static String date2Str(Date date) {
        if (Objects.isNull(date)) {
            return "";
        }

        return getSdf().format(date);
    }


    /**
     * @param date
     * @return
     */
    public static String convert(Date date) {
        String dateStr = null;
        try {
            dateStr = getSdf().format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateStr;
    }

    /**
     * 获取当前时间
     *
     * @return
     */
    public static String currentDate() {
        return convert(new Date());
    }

    /**
     * 获取当前时间
     *
     * @return
     */
    public static String currentDate(String partten) {
        dateTimeFormats = partten;
        return convert(new Date());
    }


    /**
     * 获取之前几天的时间
     *
     * @param date
     * @param before
     * @return
     */
    public static String getBeforeTime(Date date, int before) {
        Date d = null;
        try {
            Calendar c = Calendar.getInstance();
            c.setTime(date == null ? new Date() : date);
            c.add(Calendar.DATE, -before);
            d = c.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convert(d);
    }


    /**
     * 比较两个时间的大小
     *
     * @param DATE1
     * @param DATE2
     * @return
     */
    public static int compare_date(String DATE1, String DATE2) {
        try {
            Date dt1 = getSdf().parse(DATE1);
            Date dt2 = getSdf().parse(DATE2);
            if (dt1.getTime() > dt2.getTime()) {
                return 1;
            } else if (dt1.getTime() < dt2.getTime()) {
                return -1;
            } else {
                return 0;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return 0;
    }


    public static String getYear() {
        return currentDate().substring(0, 4);
    }


    private static SimpleDateFormat getSdf() {
        if (sdf == null) {
            sdf = new SimpleDateFormat(dateTimeFormats);
        }
        return sdf;
    }

    @Override
    public Date convert(String source) {
        try {
            return getSdf().parse(source);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("bad time source " + source);
    }


}
