package com.hiup.utils;

import com.hiup.vo.PatientInfo;
import com.hiup.vo.PatientInfoVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Bean相关的工具类
 *
 * @author YFC
 * @create 2018-12-20 下午 11:47
 */
public class BeanUtil {
    private static final Logger log = LoggerFactory.getLogger(BeanUtil.class);

    private static Map<String,Object> map = null;

    static {
        map = new HashMap();
        // 可以添加映射字段，利用反射设置值。
    }

    
    /**
     *  获取属性值最大一个对象
     * @param list
     * @param fieldName
     * @param <T>
     * @return
     */
    public static <T> T getMaxFieldObj(List<T> list, Object fieldName) {
        T entity = null;
        if(Objects.isNull(fieldName)){
            return entity;
        }

        long minTime = Long.MIN_VALUE;
        for (T t : list) {
            long time = getTime(fieldName,t);
            if (time < minTime) {
                continue;
            }
            minTime = time;
            entity = t;
        }
        return entity;
    }

    /**
     * 获取属性值最大的时间
     * @param fieldName
     * @param t
     * @param <T>
     * @return
     */
    private static <T> long getTime(Object fieldName, T t) {
        long time = 0L;
        try {
            Field field = t.getClass().getDeclaredField((String) fieldName);
            field.setAccessible(true);
            Object o = field.get(t);
            if (o instanceof Date) {
                Date dateCreated = (Date) o;
                time = dateCreated.getTime();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return time;

    }

    /**
     *  利用反射获取obj 属性,设置到 clazz 实例中
     * @param obj
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T fromObjSetClazz(Class<T> clazz, Object...obj) {
        T t = null;
        if(clazz == null || obj == null){
            return null;
        }
        try {
            t = clazz.newInstance();
            Field[] fields =t .getClass().getDeclaredFields();
            for(Object o: obj){
                Field[] fromFields = o.getClass().getDeclaredFields();
                for (Field field : fields) {
                    for (Field fromField : fromFields) {
                        if(field.getName().equals(fromField.getName()) ||
                                field.getName().equals(map.get(fromField.getName()))){
                            field.setAccessible(true);
                            fromField.setAccessible(true);// 设置属性可访问
                            Object val = fromField.get(o);
                            field.set(t,val);
                        }
                    }
                }
            }
        } catch (InstantiationException e) {
            log.error("Instantiation error",e.getMessage(),e);
        } catch (IllegalAccessException e) {
            log.error("No permission access error",e.getMessage(),e);
        }
        return t;
    }

    public static void main(String[] args) {
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setAccessToken("凭证2333");
        PatientInfoVo patientInfoVo = fromObjSetClazz(PatientInfoVo.class, patientInfo);
        System.out.println(patientInfoVo);

    }


}
