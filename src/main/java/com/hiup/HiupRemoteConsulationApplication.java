package com.hiup;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@MapperScan("com.*.mapper")
//@PropertySource(value = {"classpath:config.properties"},encoding = "UTF-8",ignoreResourceNotFound = true)
@PropertySource(value = {"file:config/application.properties", "file:config/application-db.properties", "file:config/config.properties"}, encoding = "UTF-8", ignoreResourceNotFound = true)
public class HiupRemoteConsulationApplication {
    private static final Logger log = LoggerFactory.getLogger(HiupRemoteConsulationApplication.class);

    public static void main(String[] args) {
        try {
            SpringApplication.run(HiupRemoteConsulationApplication.class, args);
            log.info("启动springBoot成功.");
        } catch (Exception e) {
            log.error("启动springBoot失败.", e.getMessage(), e);
        }
    }

}

