package com.hiup.config;

import com.hiup.config.comment.CustomDomainComment;
import com.hiup.config.comment.InterComment;
import com.hiup.config.comment.RecordComment;
import com.hiup.config.comment.RemoteComment;
import com.hiup.test.Tests;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author YFC
 * @create 2018-12-19 下午 10:18
 */
public class ConfigTest extends Tests{
    private static final Logger log = LoggerFactory.getLogger(ConfigTest.class);

    @Autowired
    private RecordComment recordComment;
    @Autowired
    private RemoteComment remoteComment;
    @Autowired
    private InterComment interComment;
    @Autowired
    private CustomDomainComment customDomainComment;

    @Test
    public void test() {
        log.info("测试获取配置文件.....");
        log.info("{}", recordComment);
        log.info("{}", remoteComment);
        log.info("{}", interComment);
        log.info("{}", customDomainComment);
    }

}
