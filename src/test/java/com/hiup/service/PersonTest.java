package com.hiup.service;

import com.hiup.config.DbContextHolder;
import com.hiup.constant.DBTypeEnum;
import com.hiup.entity.Person;
import com.hiup.service.impl.PersonServiceImpl;
import com.hiup.test.Tests;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author YFC
 * @create 2018-12-19 下午 10:44
 */
public class PersonTest extends Tests {

    @Autowired
    private PersonService personService;

    @Autowired
    private PersonServiceImpl personServiceImpl;

    @Test
    public void testGet() {
        DbContextHolder.setDbType(DBTypeEnum.db2);
        String personId = "5334770";
        Person person = personService.selectById(personId);
        Assert.assertNotNull(person);
    }


    // 测试根据身份证号 + 配置文件获取的门诊域id | 住院域 id
    @Test
    public void testGetPersonByIdNumAndHospitalId() {
        String idNum = "442530196709300016";      // 从 consult 中获取
        String hospitalId = "2.16.840.1.113883.4.487.2.1.4"; // 从配置文件中获取
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("identity_no", idNum);
        map.put("custom11", hospitalId);
        DbContextHolder.setDbType(DBTypeEnum.db2);
        List<Person> personList = personService.selectByMap(map);
        Assert.assertNotNull(personList);
    }


    @Test
    public void testSelectOne() {
        Map<String, Object> map = new HashMap<>();
        map.put("identity_No", "12222");
        map.put("custom11", "1222");
        Person person = personServiceImpl.selectOne(map);
        Assert.assertNotNull(person);
    }


}
