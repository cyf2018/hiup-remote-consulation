package com.hiup.service;

import com.hiup.config.DbContextHolder;
import com.hiup.constant.DBTypeEnum;
import com.hiup.entity.TblRemoteConsult;
import com.hiup.test.Tests;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

/**
 * @author YFC
 * @create 2018-12-19 上午 10:13
 */
public class TblConsultTest extends Tests {

    @Autowired
    private TblRemoteConsultService tblRemoteConsultService;

    @Before
    public void before() {
        DbContextHolder.setDbType(DBTypeEnum.db1);
    }


    @Test
    public void testGet() {
        String id = "af430e11-94f4-444a-8c93-22b228b0a04e";
        TblRemoteConsult tblRemoteConsult = tblRemoteConsultService.selectById(id);
        Assert.assertNotNull(tblRemoteConsult);
    }


    @Test
    public void testSaveConsult() {
        TblRemoteConsult consult = new TblRemoteConsult();
        String id = UUID.randomUUID().toString();
        consult.setId(id);
        consult.setAccessToken("凭证");
        consult.setHospitalId("10001");
        consult.setHospitalName("医疗机构名称");
        consult.setIdentityId("360731199904110000");
        consult.setApplicationId("applicationId");
        consult.setApplicationName("applicationName");
        tblRemoteConsultService.insert(consult);

        TblRemoteConsult consult1 = tblRemoteConsultService.selectById(id);
        Assert.assertNotNull(consult1);
    }

}
