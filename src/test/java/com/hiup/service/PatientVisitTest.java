package com.hiup.service;

import com.hiup.config.DbContextHolder;
import com.hiup.constant.DBTypeEnum;
import com.hiup.entity.PatientVisit;
import com.hiup.test.Tests;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 单元测试遵守AIR原则
 * A Automatic (自动化)
 * I Independent (独立性)
 * R Repeatable (可重复)
 *
 * @author YFC
 * @create 2018-12-19 下午 10:51
 */
public class PatientVisitTest extends Tests {


    @Autowired
    private PatientVisitService patientVisitService;

    @Before
    public void before() {
        DbContextHolder.setDbType(DBTypeEnum.db2);
    }


    @Test
    public void testGet() {
        String visitId = "13567490045005";
        PatientVisit patientVisit = patientVisitService.selectById(visitId);
        Assert.assertNotNull(patientVisit);
    }


    // 根据personId 获取 patientVisit 对象
    @Test
    public void testGetPatientVisitByPersonId() {
        String personId = "7916702541094";
        Map<String, Object> map = new HashMap();
        map.put("person_id", personId);
        List<PatientVisit> patientVisits = patientVisitService.selectByMap(map);
        Assert.assertNotNull(patientVisits);
    }


}
