package com.hiup.test;

import com.hiup.HiupRemoteConsulationApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author cyf
 * @description
 * @create 2018-12-24 14:14
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HiupRemoteConsulationApplication.class)
public class Tests {
}
