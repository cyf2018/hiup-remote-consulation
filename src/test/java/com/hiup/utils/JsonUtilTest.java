package com.hiup.utils;

import org.junit.Test;

/**
 * json工具类测试
 *
 * @author YFC
 * @create 2018-12-21 上午 1:54
 */
public class JsonUtilTest {

    @Test
    public void testJsonToObj(){
//        String jsonStr = "{\"studyList\":[{\"patientID\":\"影像号\",\"accNum\":\"存取编号\",\"studyUid\":\"检查号\",\"AE\":\"前置机AETitle\",\"IP\":\"前置机IP\",\"port\":\"关联ID\",\"examType\":\"检查类型\",\"examBodyPart\":\"检查部位\",\"examMethod\":\"检查方法\"},{...}],\"resultCode\":\"0成功,1失败\",\"errorMsg\":\"错误信息，成功无内容\"}";
//        ImageVo imageVo = JSONUtil.jsonToPojo(jsonStr.replaceAll("\\*",""), ImageVo.class);
//        //转json需要属性名称保持一致吗。要不转换的时候会报
//        // org.codehaus.jackson.map.exc.UnrecognizedPropertyException: Unrecognized field "AE" (Class com.hiup.vo.ImageDataVo), not marked as ignorable
//        Assert.assertNotNull(imageVo);
    }

    public static void main(String[] args) {

        String str = "{\n" +
                "    \"studyList\":\n" +
                "\t[\n" +
                "\t\t{\n" +
                "\t\t\t\"patientID\":\"影像号\",\n" +
                "\t\t\t\"accNum\":\"存取编号\",\n" +
                "\t\t\t\"studyUid\":\"检查号\",\n" +
                "\t\t\t\"AE\":\"前置机AETitle\",\n" +
                "\t\t\t\"IP\":\"前置机IP\",\n" +
                "\t\t\t\"port\":\"关联ID\",\n" +
                "\t\t\t\"examType\":\"检查类型\",\n" +
                "\t\t\t\"examBodyPart\":\"检查部位\",\n" +
                "\t\t\t\"examMethod\":\"检查方法\"\n" +
                "\t\t},\n" +
                "\t\t{...}\n" +
                "\t],\n" +
                "\t\"resultCode\":\"0成功,1失败\",\n" +
                "\t\"errorMsg\": \"错误信息，成功无内容\"\n" +
                "}";
        System.out.println(str.replaceAll("\n","")
        .replaceAll("\t","")
        .replaceAll(" ","")
        .trim());
    }



}
