package com.hiup.utils;

import com.hiup.constant.GlobalConstant;
import com.hiup.entity.Person;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 测试工具类
 *
 * @author YFC
 * @create 2018-12-20 下午 11:53
 */
public class BeanUtilTest {

    @Test
    public void test() {
        List<Person> personList = new ArrayList<>();
        Person person = new Person();
        person.setDateCreated(new Date());
        Person person1 = new Person();
        String beforeTime = DateUtil.getBeforeTime(new Date(), 5);
        person1.setDateCreated(DateUtil.str2Date(beforeTime));
        personList.add(person);
        personList.add(person1);
        person = BeanUtil.getMaxFieldObj(personList, GlobalConstant.DATE_CREATED);
        Assert.assertNotNull(person);
    }


}
