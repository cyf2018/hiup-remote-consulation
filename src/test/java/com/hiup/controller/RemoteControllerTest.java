package com.hiup.controller;

import com.hiup.config.DbContextHolder;
import com.hiup.constant.DBTypeEnum;
import com.hiup.constant.ResponseBean;
import com.hiup.entity.Person;
import com.hiup.entity.TblRemoteConsult;
import com.hiup.service.PersonService;
import com.hiup.service.TblRemoteConsultService;
import com.hiup.test.Tests;
import com.hiup.utils.JSONUtil;
import com.hiup.vo.ImageVo;
import com.hiup.vo.PatientDocumentVo;
import com.hiup.vo.PatientInfoVo;
import com.hiup.web.RemoteController;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.UUID;

/**
 * 远程接口controller 测试
 *
 * @author cyf
 * @description
 * @create 2018-12-24 16:24
 **/
public class RemoteControllerTest extends Tests {
    private static final Logger log = LoggerFactory.getLogger(RemoteControllerTest.class);

    /**
     * 判断是否保存成功
     */
    private static final boolean SAVE_SUCCESS = true;

    @Autowired
    private RemoteController remoteController;
    @Autowired
    private TblRemoteConsultService consultService;
    @Autowired
    private PersonService personService;

    @Test
    public void testRemind() {
        TblRemoteConsult consult = new TblRemoteConsult();
        String id = UUID.randomUUID().toString();
        log.info("consult id is {}", id);
        consult.setId(id);
        consult.setAccessToken("凭证");
        consult.setHospitalId("10001");
        consult.setHospitalName("医疗机构名称");
        consult.setIdentityId("360731199904110000");
        consult.setApplicationId("applicationId");
        consult.setApplicationName("applicationName");
        boolean saveSuccess = consultService.insert(consult);
        Assert.assertEquals(saveSuccess, SAVE_SUCCESS);
    }


    @Test
    public void testPushInfoRemote() {
        DbContextHolder.setDbType(DBTypeEnum.db1);
        String id = "41a10c24-bceb-4d2b-a65d-988e0e7f2cab";
        TblRemoteConsult consult = consultService.selectById(id);
        ResponseBean responseBean = remoteController.pushInfoToRemote(consult);
        Assert.assertNotNull(responseBean);
        log.info("推送患者信息和病历信息到远程会诊,返回结果是: {}", responseBean);
    }

    @Test
    public void testGetPerson() {
        DbContextHolder.setDbType(DBTypeEnum.db1);
        String id = "41a10c24-bceb-4d2b-a65d-988e0e7f2cab";
        TblRemoteConsult consult = consultService.selectById(id);
        Person person = remoteController.getPerson(consult);
        Assert.assertNotNull(person);
    }

    @Test
    public void testGetInhosNoAndFlowId() {
        DbContextHolder.setDbType(DBTypeEnum.db2);
        String personId = "5334770";
        Person person = personService.selectById(personId);
        Map<String, String> inhosNoAndFlowId =
                remoteController.getInhosNoAndFlowId(person);
        Assert.assertNotNull(inhosNoAndFlowId);
        log.info("参数对象是 : {}", JSONUtil.objectToJson(inhosNoAndFlowId));
    }


    @Test
    public void testGetImageVo() {
        DbContextHolder.setDbType(DBTypeEnum.db2);
        String personId = "5334770";
        Person person = personService.selectById(personId);
        // 测试获取影像接口数据前，需要确定影像接口程序开启或者
        // 自行添加影像接口样例数据,在JsonUtils.main() 中可查找样例数据.
        ImageVo imageVo = remoteController.getImageVo(person);
        Assert.assertNotNull(imageVo);
        log.info("testGetImageVo result is {}", imageVo);
    }

    @Test
    public void testGetPathologyParams() {
        String personId = "5334770";
        DbContextHolder.setDbType(DBTypeEnum.db2);
        Person person = personService.selectById(personId);
        Map<String, Object> pathologyParams = remoteController.getPathologyParams(person);
        Assert.assertNotNull(pathologyParams);
        log.info("病例数据参数对象是 : {}", JSONUtil.objectToJson(pathologyParams));
    }


    @Test
    public void testGetPatientInfo() {
        DbContextHolder.setDbType(DBTypeEnum.db1);
        TblRemoteConsult consult = consultService.selectById("af430e11-94f4-444a-8c93-22b228b0a04e");
        consult.setHospitalId("10001");
        PatientInfoVo patientInfo = remoteController.getPatientInfo(consult);
        Assert.assertNotNull(patientInfo);
    }

    /**
     * ******  测试获取患者病理文书信息
     */
    @Test
    public void testGetPatientDocument() {
        DbContextHolder.setDbType(DBTypeEnum.db1);
        TblRemoteConsult consult = consultService.selectById("af430e11-94f4-444a-8c93-22b228b0a04e");
        consult.setHospitalId("10001");
        PatientInfoVo patientInfo = remoteController.getPatientInfo(consult);
        PatientDocumentVo patientDocument = remoteController.getPatientDocument(consult, patientInfo);
        Assert.assertNotNull(patientDocument);
    }

}
